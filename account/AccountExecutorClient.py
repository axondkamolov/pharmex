from modules.kinetic_core.AbstractClient import AbstractClient, rpc
from account.AccountExecutor import AccountExecutor
from modules.kinetic_core.AbstractExecutor import executor


@executor(AccountExecutor)
class AccountExecutorClient(AbstractClient):
    pass
