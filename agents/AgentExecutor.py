from modules.kinetic_core.AbstractExecutor import *
from orderingservice.OrderingServiceExecutorClient import OrderingServiceExecutorClient
from preorderingservice.PreorderingServiceExecutorClient import PreorderingServiceExecutorClient


class AgentExecutor(AbstractExecutor):
    def __init__(self, task):
        super().__init__(task)
        self.params = {"state": None}

    async def login(self, data):
        print(self.data)
        origin = self.data["origin"]
        login = self.data["login"]
        password = self.data["password"]

        if origin is None:
            return self._error("origin required[driver_app, warehouse_app, control_panel]")

        if login is None:
            return self._error("login required")

        if password is None:
            return self._error("password required")

        user = await self._validate(login, password)
        if user is None:
            return self.error("invalid credential")
        else:
            print("publish")
            print(user)
            await self.publish(user)

    async def logout(self, data):
        pass

    async def _validate(self, login, password):
        asyncio.sleep(1)
        if login == "test" and password == "test":
            return {"status": "success", "data": {"roles": ["put", "pick", "pack", "ship"], "token": "alsa"}}

    @staticmethod
    async def _error(message):
        return {"status": "fail", "message": message}

    async def get_delivery_locations(self, agent_id):
        # TODO: retrieve agent delivery locations from db
        # agent = await self.get(["a" + str(agent_id)])
        # locations = agent['delivery_locations']
        locations = {
            'Chilonzor': False,
            'Yunusobod': True,
            'MirzoUlugbek': True,
            'Xamza': False,
            'Mirobod': True
        }

        return locations

    async def get_status(self, agent_id):
        # agent = await self.get(["a" + str(agent_id)])
        # status = agent['status']
        status = False
        return status

    async def process(self, data):
        """
        Accepts command and sends to executors
        Structure:
            command: command to execute
            source: source of command
            source: id of source
            destination: not necessary for agent exec.
            destination_id: not necessary for agent exec.
            payload: {} json data for command
        :param data: json structure
        :return: returns result for command
        """
        print("Process in AgentExecutor")
        command = data['command']
        payload = data['payload']
        agent_id = payload['agent_id']

        # inner executors
        if command is 'SendPreOrder':
            preorder_executor = PreorderingServiceExecutorClient()
            command_result = await preorder_executor.preorder(data=payload)
            self.params['state'] = "preordering"
            await self.save({'a' + str(agent_id): self.params})
            return command_result
        if command is 'ConfirmOrder':
            self.params['state'] = "confirming"
            command_result = await self.save({'a' + str(agent_id): self.params})
            return command_result
        if command is 'SendOrder':
            order_executor = OrderingServiceExecutorClient()
            command_result = await  order_executor.order(data=payload)
            self.params['state'] = "ordering"
            await self.save({'a' + str(agent_id): self.params})
            return command_result
        if command is 'new_offer':
            """
            call web/mobile executor
            """
        if command is 'cancel_offer':
            """
            call web/mobile executor
            """
        if command is 'send_notification':
            """
            call web/mobile executor
            """
        if command is 'get_cabinet_info':
            """
            DB comm.
            """
        if command is 'get_settings':
            """
            DB comm.
            """
        if command is 'modify_settings':
            """
            DB comm.
            """
