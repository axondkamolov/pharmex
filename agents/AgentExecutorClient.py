from modules.kinetic_core.AbstractClient import *
from modules.kinetic_core.AbstractExecutor import executor
from agents.AgentExecutor import AgentExecutor


@executor(AgentExecutor)
class AgentExecutorClient(AbstractClient):

    @rpc
    async def login(self, data):
        """
        Login into the system
        :param data: массив содержаний lat, lon и идентификатор
        :return: оптимизированный по расстоянию маршрут
        """
        pass

    @rpc
    async def logout(self, data):
        """
        Оптимизирует входящие данные
        :param data: массив содержаний lat, lon и идентификатор
        :return: оптимизированный по расстоянию маршрут
        """
        pass

    @rpc
    async def get_delivery_locations(self):
        """
        Returns supplier/agent id with locations to which he can deliver
        :return: json data with locations
        """
        pass

    @rpc
    async def get_status(self):
        """
        Returns current status of agent/supplier
        :return: json data with current status 'blocked":True/False
        """
        pass

    @rpc
    async def process(self, data):
        """
        Accepts command and sends to executors
        Structure:
            command: command to execute
            source: source of command
            source: id of source
            destination: not necessary for agent exec.
            destination_id: not necessary for agent exec.
            payload: {} json data for command
        :param data: json structure
        :return: returns result for command
        """
        pass
