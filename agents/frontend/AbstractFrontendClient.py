from modules.kinetic_core.AbstractClient import AbstractClient
from modules.kinetic_core.AbstractClient import rpc


class AbstractFrontendClient(AbstractClient):

    @staticmethod
    def factory(t):
        if t == "web":
            from agents.frontend.web.AgentWebClient import AgentWebClient
            return AgentWebClient()
        if t == "mobile":
            from agents.frontend.mobile.AgentMobileClient import AgentMobileClient
            return AgentMobileClient()

    @rpc
    async def send_preorder(self, data):
        pass

    @rpc
    async def confirm_order(self, order_id):
        pass

    @rpc
    async def send_order(self, order_id):
        pass
