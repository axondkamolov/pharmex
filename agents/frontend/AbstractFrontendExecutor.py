from modules.kinetic_core.AbstractExecutor import AbstractExecutor


class AbstractFrontendExecutor(AbstractExecutor):

    async def send_preorder(self, data):
        pass

    async def confirm_order(self, order_id):
        pass

    async def send_order(self, order_id):
        pass