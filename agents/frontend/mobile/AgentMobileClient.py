from agents.frontend.AbstractFrontendClient import AbstractFrontendClient
from agents.frontend.mobile.AgentMobileExecutor import AgentMobileExecutor
from modules.kinetic_core.AbstractExecutor import executor


@executor(AgentMobileExecutor)
class AgentMobileClient(AbstractFrontendClient):
    pass
