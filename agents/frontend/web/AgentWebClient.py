from agents.frontend.AbstractFrontendClient import AbstractFrontendClient
from agents.frontend.web.AgentWebExecutor import AgentWebExecutor
from modules.kinetic_core.AbstractExecutor import executor


@executor(AgentWebExecutor)
class AgentWebClient(AbstractFrontendClient):
    pass
