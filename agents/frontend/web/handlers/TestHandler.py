import json

import tornado.web

from agents.AgentExecutorClient import AgentClient


class TestHandler(tornado.web.RequestHandler):

    async def get(self):
        self.render("test/main.html")

    async def post(self):
        payload = self.get_argument('payload', 'No data received')
        agent_client = AgentClient()
        result = await agent_client.process(data=json.loads(payload))
        self.write(str(result))
        self.finish()
