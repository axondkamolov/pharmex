import asyncio
import json
import logging
import os

import tornado.concurrent
import tornado.ioloop
import tornado.locks
import tornado.web
import tornado.platform.asyncio
import tornado.httpclient

from agents.frontend.web.handlers.TestHandler import TestHandler
from modules.kinetic_core import Logger

loop = asyncio.get_event_loop()

# Инициируем логгирование
Logger.init(logging.DEBUG)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/test", TestHandler),
        ]
        settings = dict(
            blog_title=u"Web",
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
            cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
            login_url="/auth/login",
            debug=True,
        )
        super(Application, self).__init__(handlers, **settings)


async def main():
    app = Application()
    print("started at: 8889")
    app.listen(8889)
    shutdown_event = tornado.locks.Event()
    await shutdown_event.wait()


if __name__ == "__main__":
    tornado.ioloop.IOLoop.current().run_sync(main)
