import asyncio
import logging

from agents.AgentExecutorClient import AgentExecutorClient
from agents.AgentExecutor import AgentExecutor
from modules.kinetic_core import Logger
from modules.kinetic_core.AbstractExecutor import *
from modules.kinetic_core.QueueListener import QueueListener

# главный луп приложения
main_loop = asyncio.get_event_loop()

# Инициируем логгирование
Logger.init(logging.DEBUG)


# Регистрируем наш слушатель - он принимает сообщения из очереди rabbitMQ
@executor(AgentExecutor)
class AgentQueueListener(QueueListener):

    async def parse(self, task):
        print('Listening')
        await AgentExecutor(task).parse()


async def do_task():
    basket = {
        101: {"qty": 1000, "exp": datetime.now()},
        102: {"qty": 1500, "exp": datetime.now()},
        114: {"qty": 1000, "exp": datetime.now()}
    }
    payload = {'agent_id': 1, 'basket': basket}
    command = 'SendPreOrder'
    data = {'command': command, 'payload': payload}
    agent_executor = AgentExecutorClient()
    result = await agent_executor.process(data=data)

    print(result)


main_loop.create_task(do_task())
main_loop.create_task(AgentQueueListener().register_listener(main_loop))
main_loop.run_forever()
