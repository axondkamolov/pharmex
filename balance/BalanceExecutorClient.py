from modules.kinetic_core.AbstractClient import AbstractClient, rpc
from modules.kinetic_core.AbstractExecutor import executor
from balance.BalanceExecutor import BalanceExecutor


@executor(BalanceExecutor)
class BalanceExecutorClient(AbstractClient):

    @rpc
    async def get_balance(self, agent_id):
        '''
        gets agent balance information
        rmk: called from account executor
        :param agent_id:
        :return:
        '''
        pass
