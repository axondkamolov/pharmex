import logging
from datetime import datetime
from modules.kinetic_core.AbstractExecutor import *
from booking.BookingExecutorDB import *

from warehouse.WarehouseExecutorClient import WarehouseExecutorClient
from warehouse.StockExecutorClient import StockExecutorClient
from shipment.ShipmentExecutorClient import ShipmentExecutorClient

class BookingExecutor(AbstractExecutor):

    def __init__(self, task):
        super().__init__(task)
        self.uid = "booking_id"
        self.params = {"booking_id": None,
                       }


    async def book_basket(self):
        # RPC TYPE
        # This method executes the following logic:
        # - check if whole order is available in all of warehouses
        # - if yes, use an optimizator to select the warehouse with lowest marginal cost
        # - if no, choose the one which is available
        # - if none of warehouses is available for single batch shipment, compose separate shipments from different warehouses
        # - makes booking as a result of logic outcome - booking modified qunantities and creates next necessary executors

        # - check if whole order is available in all of warehouses

        stock_executor = StockExecutorClient(asyncio.get_event_loop())
        product_availability = {}
        for product_id in self.data["basket"]:
            stock_by_product = await stock_executor.get_one({"product_id": product_id})
            product_availability[product_id] = stock_by_product

        warehouse_executor = WarehouseExecutorClient(asyncio.get_event_loop())
        warehouses = await warehouse_executor.get_all(None)
        product_availability_by_warehouse = {}
        for w in warehouses:
            warehouse_id = w["warehouse_id"]
            product_availability_in_warehouse = {}
            for product_id in self.data["basket"]:
                racks = product_availability[warehouse_id]
                quantity_in_warehouse = 0
                for rack_id in racks:
                    quantity_in_rack = racks[rack_id]
                    quantity_in_warehouse += quantity_in_rack
                product_availability_in_warehouse[product_id] = quantity_in_warehouse
            product_availability_by_warehouse[w] = product_availability_in_warehouse

        basket_availability_by_warehouse = {}
        for w in warehouses:
            fully_available = True
            for product_id in self.data["basket"]:
                demanded_quantity = self.data["basket"][product_id]
                available_quantity = product_availability_by_warehouse[w][product_id]
                if available_quantity < demanded_quantity:
                    fully_available = False
            if fully_available:
                basket_availability_by_warehouse[w] = True

        if len(basket_availability_by_warehouse) > 0:
            if len(basket_availability_by_warehouse)>1:
                pass # select among warehouses using marginal cost of shipping obtained via optimizer
                selected_warehouse_id = 1
            else:
                pass # ship from one warehouse
                selected_warehouse_id = None
                for w in basket_availability_by_warehouse:
                    selected_warehouse_id = w

        else:
            pass # whole basket is not available in any warehouse, so ship by product or product batches

            shipment_executor = ShipmentExecutorClient(asyncio.get_event_loop())

            products_to_be_shipped = self.data["basket"].copy()
            for w in product_availability_by_warehouse:
                warehouse_order = {}
                for product_id in products_to_be_shipped:
                    demanded_quantity = products_to_be_shipped[product_id]
                    if demanded_quantity > 0:
                        warehouse_order[product_id] = product_availability_by_warehouse[w]
                        products_to_be_shipped[product_id] -= product_availability_by_warehouse[w]
                # create new shipment order using warehouse_order[w]
                shipment_seed = {"basket": warehouse_order}
                await shipment_executor.add(None)



        print("BookingExecutor - reserve basket")