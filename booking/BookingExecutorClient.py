from booking.BookingExecutor import BookingExecutor
from modules.kinetic_core.AbstractClient import *
from modules.kinetic_core.AbstractExecutor import executor


@executor(BookingExecutor)
class BookingExecutorClient(AbstractClient):

    @event
    def book_basket(self, data):
        """
        Reserves each item in a basket
        :param data: Basket Dict
        :return: None
        """
        pass

    @rpc
    def check_availibility(self, data):
        """
        Checks item availability in warehouse(s)
        :param data: Item Dict
        :return: Item Dict with availability indication
        """