from modules.kinetic_core.Connector import db
import json

async def db_check_availability(data):

    quantity_in_stock = (await db.query('select sum(quantity) as quantity from stock where product_id=$1', (data["product_id"])))["order_id"]
    available_next_day = (await db.query('select available_next_day from products where product_id=$1', (data["product_id"])))["order_id"]

    return {"quantity_in_stock": quantity_in_stock,
            "available_next_day": available_next_day}
