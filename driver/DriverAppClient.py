from modules.kinetic_core.AbstractClient import event
from modules.kinetic_core.AbstractMobileClient import AbstractMobileClient


class DriverAppClient(AbstractMobileClient):

    @event
    async def trip_completed(self, data):
        pass

    @event
    async def order_items(self, data):
        pass

    @event
    async def list_orders(self, data):
        pass

    @event
    async def send_status(self, data):
        pass

    @event
    async def arrived(self, data):
        pass

    @event
    async def verify_order(self, data):
        pass

    @event
    async def ready_to_give(self, data):
        pass

    @event
    async def get_status(self, data):
        pass

    @event
    async def set_location(self, data):
        pass

    @event
    async def request_driver(self, data):
        pass

    @event
    async def update_route(self, data):
        pass

    @event
    async def finish_order(self, data):
        pass
