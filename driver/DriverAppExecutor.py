from driver.DriverAppClient import DriverAppClient
from driver.DriverClient import DriverClient
from driver.DriverTripClient import DriverTripClient
from modules.kinetic_core.AbstractExecutor import client
from modules.kinetic_core.AbstractMobileExecutor import AbstractMobileExecutor


@client(DriverAppClient)
class DriverAppExecutor(AbstractMobileExecutor):

    def __init__(self, task, webSocketConnector):
        super().__init__(task, webSocketConnector)
        self.driver_id = self.data["uid"]

    async def trip_completed(self):
        self.data["type"] = "accept_trip_complete"
        await self._send(self.driver_id, self.data)

    async def get_status(self):
        driver_client = DriverClient()
        status = await driver_client.get_status(self.driver_id)
        await self._send(self.driver_id, {"type": "status", "data": status})

    async def list_orders(self):
        trip_client = DriverTripClient()
        roster = await trip_client.list_orders({"driver_id": self.data["driver_id"]})
        await self._send(self.driver_id, roster)

    async def order_items(self):
        trip_client = DriverTripClient()
        order_items = await trip_client.order_items(
            {"order_id": self.data["order_id"],
             "trip_id": self.data["trip_id"]})
        await self._send(self.driver_id, order_items)

    async def request_driver(self):
        self.data["type"] = "driver_request_packet"
        await self._send(self.driver_id, self.data)

    async def verify_order(self):
        await self._send(self.driver_id,
                         {"type": "set_client_confirm",
                          "data": {"order_id": self.data["order_id"]}})

    async def arrived(self):
        await self._send(self.driver_id,
                         {"type": "set_order_verify",
                          "data": {"order_id": self.data["order_id"]}})
        pass

    async def ready_to_give(self):
        await self._send(self.driver_id,
                         {"type": "set_client_confirm",
                          "data": {"order_id": self.data["order_id"]}})
        pass

    async def finish_order(self):
        await self._send(self.driver_id,
                         {"type": "set_return_to_warehouse",
                          "data": {"order_id": self.data["order_id"]}})
        pass

    async def update_route(self):
        self.data["type"] = "update_route"
        await self._send(self.driver_id, self.data)

    async def set_location(self):
        pass

