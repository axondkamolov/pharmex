import asyncio
import json
import logging
from abc import ABC

from driver.DriverAppClient import DriverAppClient
from modules.kinetic_core.AbstractWebSocketProtocol import AbstractWebSocketProtocol


class DriverApplicationConnector(AbstractWebSocketProtocol, ABC):

    def __init__(self):
        super().__init__()

    async def onPacket(self, text):
        d = json.loads(text)
        data = {}
        if "data" in d:
            data = d["data"]
        driver = DriverAppClient(asyncio.get_event_loop())
        token = self.http_request_params["token"]
        data["uid"] = 10

        await getattr(driver, str(d["type"]))(**data)
        logging.info(text)
