from modules.kinetic_core.AbstractClient import AbstractClient, rpc, event


class DriverClient(AbstractClient):

    @rpc
    async def add(self, data):
        """
        register agent as driver unit
        :param data:
        :return:
        """
        pass

    @rpc
    async def get_status(self, data):
        """

        :param data:
        :return:
        """
        pass

    @rpc
    async def modify_state(self, data):
        """
        change driver state("online", "offline", "preparation", "delivery")
        :param data:
        :return:
        """
        pass

    @rpc
    async def remove(self, data):
        """
        unregister driver
        :param data:
        :return:
        """
        pass
