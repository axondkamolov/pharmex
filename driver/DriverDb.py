from modules.kinetic_core.Connector import db


async def db_add(data):
    return (await db.query(
        'insert into driver_stats(agent_id, state) values '
        '($1, $2) returning driver_id, current_timestamp',
        (data["agent_id"], "offline")
    ))


async def db_modify_state(data):
    return (await db.query(
        'update driver_stats set state = $1 where driver_id = $2'
        ' returning state, current_timestamp',
        (data["driver_id"], data["state"])
    ))
    pass


async def db_remove(data):
    return (await db.query(
        'update driver_stats set active = 0 where driver_id = $1'
        ' returning active, current_timestamp',
        (data["driver_id"])
    ))
