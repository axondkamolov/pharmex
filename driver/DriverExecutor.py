from datetime import datetime

from driver.DriverClient import DriverClient
from driver.DriverDb import *
from modules.kinetic_core.AbstractExecutor import AbstractExecutor, client


@client(DriverClient)
class DriverExecutor(AbstractExecutor):

    async def add(self):
        driver_id = await db_add(self.data)
        self.data["driver_id"] = driver_id
        await self.save(driver_id, self.data)
        await self.publish(self.data)

    async def modify_state(self):
        state = await db_modify_state(self.data)
        self.data["state"] = state
        await self.save(self.data("driver_id"), self.data)
        await self.publish(self.data)

    async def get_status(self):
        r = {"state": "navigation", "queue": 4, "route": {
                "route": ["yke{FurzeL{AQ",
                          "une{FgszeLWa@Sa@Eq@Bg@@AV}CPuD`@cMMaCNqA^_CFe@R{@VkAV{@`@qAr@cBT]`@q@NWlBaCr@m@FG",
                          "_`e{Fsq|eL\\P", "a_e{Faq|eL@RDLFHHDPDb@\\t@lB", "{zd{F}j|eL"],
                "latitude": 41.317631, "longitude": 69.290964, "radius": 0},
             "order": {"order_id": 1231, "state": "moving_to_client", "address": "Дом 25, квартира 10", "client_name":
                 "Антон", "comment": "Комментарий клиента", "phone": "+998909373921", "phone2": "+998712536710",
                       "time": datetime.now()}
             }
        return r

    async def remove(self):
        pass
