import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')
from modules.kinetic_core.AbstractClient import AbstractClient, rpc, event


class DriverTripClient(AbstractClient):

    @rpc
    async def add(self):
        pass

    @rpc
    async def list_orders(self):
        pass

    @rpc
    async def add_orders(self, trip_id, order_ids):
        pass

    @event
    async def remove_order(self):
        pass

    @event
    async def order_items(self):
        pass

    @rpc
    async def remove(self):
        pass
