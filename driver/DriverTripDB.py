from modules.kinetic_core.Connector import db
import json

async def db_add(data):

    basket = json.dumps(data["basket"])

    return (await db.query(
        'insert into driver_trip(customer_id, basket, bill_basket, payment_method, payment_confirmed,'
        ' shipment_type, order_date, address_id, latitude, longitude, address_string, comment, bill_shipment, bill_total, order_status) values '
        '($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15) returning order_id, current_timestamp',
        (data["customer_id"], basket, data["bill_basket"], data["payment_method"], data["payment_confirmed"],
         data["shipment_type"], data["order_date"], data["address_id"], data["latitude"], data["longitude"], data["address_string"],
         data["comment"], data["bill_shipment"], data["bill_total"], data["order_status"])))

async def db_remove():
    pass

async def db_get_quantity(data):

    await db.query("select quantity from stock where product_id=$1", ())