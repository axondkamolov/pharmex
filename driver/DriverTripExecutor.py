from datetime import datetime

from driver.DriverTripClient import DriverTripClient
from driver.DriverTripDB import *
from modules.kinetic_core.AbstractExecutor import AbstractExecutor, client


@client(DriverTripClient)
class DriverTripExecutor(AbstractExecutor):

    async def add(self):
        trip_id = await db_add(self.data)
        self.data["trip_id"] = trip_id
        await self.save(trip_id, self.data)
        return self.data

    async def list_orders(self):
        st = datetime.now()
        data = [{"client_name": "Андрей", "time": st, "state": "finished"},
                {"client_name": "Антон", "time": st, "state": "current"},
                {"client_name": "Аброр", "time": st, "state": "future"},
                {"client_name": "Асрор", "time": st, "state": "future"}]
        return {"type": "list_orders", "data": data}

    async def order_basket(self):
        data = [{"name": "Цитрамон", "price": 2000, "count": 2},
                {"name": "Цинепар", "price": 1500, "count": 5},
                {"name": "Ацетилсалициловая кислота", "price": 3000, "count": 4},
                {"name": "L-лизина эсцинат р-р 0,1% 5мл №10", "price": 200,
                 "count": 999}]
        return {"type": "list_order_items", "data": data}

    async def add_orders(self, trip_id, order_ids):
        #trip_id = await db_add_order(self.data)
        # todo add orders to database
        return {"ok": order_ids}

    async def remove_order(self):
        # todo mark order as cancelled
        pass

    async def remove(self):
        return self.data
