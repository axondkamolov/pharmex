import asyncio
import logging

from driver.DriverApplicationConnector import DriverApplicationConnector
from driver.DriverAppClient import DriverAppClient
from driver.DriverAppExecutor import DriverAppExecutor
from driver.DriverClient import DriverClient
from driver.DriverExecutor import DriverExecutor
from driver.DriverTripClient import DriverTripClient
from driver.DriverTripExecutor import DriverTripExecutor
from modules.kinetic_core import Logger
from modules.kinetic_core.AbstractExecutor import client
from modules.kinetic_core.QueueListener import QueueListener

# главный луп приложения
from modules.kinetic_core.WebSocketConnector import WebSocketConnector

main_loop = asyncio.get_event_loop()

# Инициируем логгирование
Logger.init(logging.DEBUG)

# инициализируем коннектор websocket
webSocketConnector = WebSocketConnector()
webSocketConnector.connect(main_loop, DriverApplicationConnector)


# Регистрируем наш слушатель - он принимает сообщения из очереди rabbitMQ
@client(DriverAppClient)
class DriverAppQueueListener(QueueListener):

    async def parse(self, task):
        await DriverAppExecutor(task, webSocketConnector).parse()


@client(DriverTripClient)
class DriverTripQueueListener(QueueListener):

    async def parse(self, task):
        await DriverTripExecutor(task).parse()


@client(DriverClient)
class DriverQueueListener(QueueListener):

    async def parse(self, task):
        await DriverExecutor(task).parse()


main_loop.create_task(DriverTripQueueListener().register_listener(main_loop))
main_loop.create_task(DriverAppQueueListener().register_listener(main_loop))
main_loop.create_task(DriverQueueListener().register_listener(main_loop))
main_loop.run_forever()
