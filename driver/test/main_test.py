#!/usr/bin/python3.6

import asyncio
import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')
from driver.DriverTripClient import DriverTripClient


async def main(loop):
    picker = DriverTripClient()
    #await picker.request_driver({"uid": 20, "capacity": 50})
    # await picker.update_route({"uid": 10, "data": {
    #     "route": ["yke{FurzeL{AQ", "une{FgszeLWa@Sa@Eq@Bg@@AV}CPuD`@cMMaCNqA^_CFe@R{@VkAV{@`@qAr@cBT]`@q@NWlBaCr@m@FG",
    #               "_`e{Fsq|eL\\P", "a_e{Faq|eL@RDLFHHDPDb@\\t@lB", "{zd{F}j|eL"],
    #     "latitude": 41.317631, "longitude": 69.290964, "radius": 0
    #     }})
    trip_id = 10
    result = await picker.add_orders(trip_id=trip_id, order_ids=[1, 2, 3])
    print(result)

loop = asyncio.get_event_loop()
#loop.run_until_complete(firebase_push(loop))
#loop.run_until_complete(firebase_broadcast(loop))
loop.run_until_complete(main(loop))
#loop.run_until_complete(main(loop))
