import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')
from modules.kinetic_core.AbstractExecutor import *
from filterservice.FilterServiceExecutorDB import *

class FilterServiceExecutor(AbstractExecutor):
    def __init__(self, task):
        super().__init__(task)

    async def get_filtered_list(self, data):
        """Filter pricelist and save to DB"""
        self.data = {}
        self.data['basket_pricelist'] = data['basket_pricelist']
        self.data['basket'] = data['basket']

        """Filter by the price types the user wants"""
        self.data['basket_pricelist'] = await self.filter_by_price_version(basket_pricelist=self.data['basket_pricelist'], price_versions=self.data['basket']['price_versions'])
        """Filter by suppliers (location, status)"""
        #suppliers = await agent_filter.filter_suppliers(data={'suppliers': await self.get_suppliers_from_basket_pricelist(basket_pricelist=basket_pricelist), 'client_id': data['client_id']})
        self.data['suppliers'] = [1, 21, 33, 45]
        self.data['basket_pricelist'] = await self.filter_by_suppliers(basket_pricelist=self.data['basket_pricelist'], suppliers=self.data['suppliers'])
        """Filter by the quantity the user wants"""
        self.data['basket_pricelist'] = await self.filter_by_qty(basket_pricelist=self.data['basket_pricelist'], basket=self.data['basket']['basket'])
        """Filter by expiry date"""
        self.data['basket_pricelist'] = await self.filter_by_exp(basket_pricelist=self.data['basket_pricelist'], basket=self.data['basket']['basket'])
        """Extract soonest expiring"""
        self.data['basket_pricelist'] = await self.get_soonest_expiring(basket_pricelist=self.data['basket_pricelist'])

        return self.data['basket_pricelist']

    async def get_suppliers_from_basket_pricelist(self, basket_pricelist):
        suppliers = set([])
        for plist_key in basket_pricelist.keys():
            for supplier_id in basket_pricelist[plist_key].keys():
                suppliers.add(supplier_id)
        return suppliers

    async def filter_by_suppliers(self, basket_pricelist, suppliers):
        for plist_key in basket_pricelist.keys():
            for supplier_id in basket_pricelist[plist_key].keys():
                if not int(supplier_id) in suppliers:
                    basket_pricelist[plist_key][supplier_id] = None
                basket_pricelist[plist_key] = {k: v for k, v in basket_pricelist[plist_key].items() if v != None}
        return basket_pricelist


    async def filter_by_qty(self, basket_pricelist, basket):
        for price_version in basket_pricelist.keys():
            for supplier_id in basket_pricelist[price_version].keys():
                for product_id in basket.keys():
                    basket_pricelist[price_version][supplier_id] = [product for product in basket_pricelist[price_version][supplier_id] if not ((product['product_id'] == product_id) & (product['qty'] < basket[product_id]['qty']))]
            basket_pricelist[price_version] = {k: v for k, v in basket_pricelist[price_version].items() if v}
        basket_pricelist = {k: v for k, v in basket_pricelist.items() if v}
        return basket_pricelist

    async def filter_by_exp(self, basket_pricelist, basket):
        for plist_key in basket_pricelist.keys():
            for supplier_id in basket_pricelist[plist_key].keys():
                for product_id in basket.keys():
                    basket_pricelist[plist_key][supplier_id] = [product for product in basket_pricelist[plist_key][supplier_id] if not ((product['product_id'] == product_id) & (product['exp'] < basket[product_id]['exp']))]
            basket_pricelist[plist_key] = {k: v for k, v in basket_pricelist[plist_key].items() if v}
        basket_pricelist = {k: v for k, v in basket_pricelist.items() if v}
        return basket_pricelist

    async def get_soonest_expiring(self, basket_pricelist):
        for plist_key in basket_pricelist.keys():
            for supplier_id in basket_pricelist[plist_key].keys():
                nearest_expiry = {}
                for product in basket_pricelist[plist_key][supplier_id]:
                    if product['product_id'] in nearest_expiry.keys():
                        if nearest_expiry[product['product_id']]['exp'] > product['exp']:
                            nearest_expiry[product['product_id']] = {k: v for k, v in product.items() if k != 'product_id'}
                    else:
                        nearest_expiry[product['product_id']] = {k: v for k, v in product.items() if k != 'product_id'}
                basket_pricelist[plist_key][supplier_id] = nearest_expiry
        return basket_pricelist

    async def filter_by_price_version(self, basket_pricelist, price_versions):
        basket_pricelist = {k: v for k, v in basket_pricelist.items() if int(k) in price_versions}
        return basket_pricelist