import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')

from modules.kinetic_core.AbstractClient import *
from modules.kinetic_core.AbstractExecutor import executor
from filterservice.FilterServiceExecutor import FilterServiceExecutor

@executor(FilterServiceExecutor)
class FilterServiceExecutorClient(AbstractClient):

    @rpc
    async def get_filtered_list(self, data):
        pass

    @rpc
    async def generate_filtered_list(self, pricelist):
        pass