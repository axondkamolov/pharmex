import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')
from modules.kinetic_core.Connector import db
import json

async def db_add_filtered_list(data):
    return (await db.query(
        'insert into filtered_lists(basket_pricelist_id, filtered_list) values '
        '($1, $2) returning filtered_list_id, current_timestamp',
        (data['basket_pricelist_id'], json.dumps(data["filtered_list"]))
    ))