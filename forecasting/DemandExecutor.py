import logging
from datetime import datetime
import copy
from datetime import timezone
from asyncio import sleep as asleep

from modules.kinetic_core.AbstractExecutor import *
from forecasting.DemandExecutorDB import *

class DemandExecutor(AbstractExecutor):

    def __init__(self, task):
        super().__init__(task)
        self.uid = "product_id"
        self.params = {"demand_date": None,
                       "demand_forecast": None,
                       "demand_actual": None,
                       "product_id": None}

    async def add(self):
        for d in self.data:
            self.params[d] = self.data[d]
        await db_add(self.params)
        timestamp = int(self.params["demand_date"].replace(tzinfo=timezone.utc).timestamp())
        await self.save({"p"+str(self.params["product_id"]): {"d"+str(timestamp): self.params}})

    async def get_one(self):
        instance_id = self.data["product_id"]
        timestamp = int(self.data["demand_date"].replace(tzinfo=timezone.utc).timestamp())
        instance = await self.get(["p"+str(instance_id),"d"+str(timestamp)])
        return instance

    async def modify(self):
        instance_id = self.data["product_id"]
        timestamp = int(self.data["demand_date"].replace(tzinfo=timezone.utc).timestamp())
        params = await self.get(["p" + str(instance_id), "d" + str(timestamp)])
        if params is None:
            logging.error("instance does not exist")
            return False
        for p in params:
            self.params[p] = params[p]
        for d in self.data:
            self.params[d] = self.data[d]
        result = await db_modify(self.params)
        for r in result:
            self.params[r] = result[r]
        await self.save({"p" + str(self.params["product_id"]): {"d" + str(timestamp): self.params}})

    async def remove(self):
        await db_remove(self.data["product_id"], self.data["demand_date"])
        timestamp = int(self.data["demand_date"].replace(tzinfo=timezone.utc).timestamp())
        await self.save({"p" + str(self.data["product_id"]): {"d" + str(timestamp): None}})
        return True

    async def forecast_demand_v1(self): # Forecast based on market share assumptions and yearly import data from customs department
        pass

    async def forecast_demand_v2(self): # Forecast based on own demand data
        pass

    async def dosmthconstantly(self):
        loop.create_task(self.smthloop())

    async def smthloop(self):
        while True:
            print("doing smth...")
            await asleep(1)