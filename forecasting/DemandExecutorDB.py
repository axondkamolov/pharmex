from modules.kinetic_core.Connector import db
import json

async def db_add(data):

    return (await db.query(
        'insert into demand(product_id, demand_date, demand_forecast, demand_actual) values '
        '($1, $2, $3, $4) returning product_id, current_timestamp',
        (data["product_id"], data["demand_date"], data["demand_forecast"], data["demand_actual"])))

async def db_remove(instance_id, demand_date):
    return (await db.query('delete from demand where product_id=$1 and demand_date=$2 returning entry_id, current_timestamp',
                           (instance_id,demand_date)))

async def db_modify(data):
    print(type(data["demand_actual"]))
    print(type(None))#TODO Anton needs to correct DuckPunching to accept None values as update
    return (await db.query('update demand set demand_forecast=$1, demand_actual=$2 where product_id=$3 and demand_date=$4 returning demand_forecast, demand_actual, current_timestamp',
        ((data["demand_forecast"], data["demand_actual"], data["product_id"], data["demand_date"]))))

