import logging
from datetime import datetime
import copy
from datetime import timezone
from asyncio import sleep as asleep

from modules.kinetic_core.AbstractExecutor import *
from forecasting.DemandYearlyExecutorDB import *

class DemandYearlyExecutor(AbstractExecutor):

    def __init__(self, task):
        super().__init__(task)
        self.uid = "product_id"
        self.params = {"product_id": None,
                       "year": None,
                       "quantity": None,
                       "jan": None,
                              "feb": None,
                              "mar": None,
                              "apr": None,
                              "may": None,
                              "jun": None,
                              "jul": None,
                              "aug": None,
                              "sep": None,
                              "oct": None,
                              "nov": None,
                              "dec": None}

    async def add(self, data):
        for d in data:
            self.params[d] = data[d]
        result = await db_add(data)
        product_id = result["product_id"]
        await self.save({"p"+str(product_id): self.params})
        return True

    async def get_one(self, data):
        params = await self.get(["p" + str(data["product_id"])])
        return params

    async def modify(self, data):
        params = await self.get(["p" + str(data["product_id"])])
        if params is None:
            logging.error("instance does not exist")
            return False
        for p in params:
            self.params[p] = params[p]
        for d in data:
            self.params[d] = data[d]
        result = await db_modify(self.params)
        for r in result:
            self.params[r] = result[r]
        await self.save({"p"+str(data["product_id"]):self.params})
        return True

    async def remove(self, data):
        await db_remove(data["product_id"])
        await self.save({"p" + str(data["product_id"]): None})
        return True