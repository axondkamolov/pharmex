from modules.kinetic_core.Connector import db
import json

async def db_add(data):
    return (await db.query('insert into demand_yearly(product_id, year, quantity, jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15) returning product_id, current_timestamp',
                           (data["product_id"], data["year"], data["quantity"], data["jan"],
                            data["feb"], data["mar"], data["apr"], data["may"], data["jun"],
                            data["jul"], data["aug"], data["sep"], data["oct"], data["nov"],
                            data["dec"])))

async def db_modify(data):
    return (await db.query('update demand_yearly set year=$1, quantity=$2, jan=$3, feb=$4, mar=$5, apr=$6, may=$7, jun=$8, jul=$9, aug=$10, sep=$11, oct=$12, nov=$13, dec=$14 where product_id=$15 returning year, quantity, product_id, jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec, current_timestamp', ((data["year"], data["quantity"], data["jan"], data["feb"], data["mar"], data["apr"], data["may"], data["jun"], data["jul"], data["aug"], data["sep"], data["oct"], data["nov"], data["dec"], data["product_id"]))))

async def db_remove(instance_id):
    return (await db.query('delete from demand_yearly where product_id=$1 returning entry_id, current_timestamp',
                           (instance_id)))
