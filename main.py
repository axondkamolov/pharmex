import asyncio
import logging
import pandas as pd
import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')
from aio_pika import Exchange, IncomingMessage

from modules.kinetic_core import Logger
from modules.kinetic_core.AbstractExecutor import executor
from modules.kinetic_core.QueueListener import QueueListener
from booking.BookingExecutor import BookingExecutor
from booking.BookingExecutorClient import BookingExecutorClient
from sales.SalesOrderExecutor import SalesOrderExecutor
from sales.SalesOrderExecutorClient import SalesOrderExecutorClient
from warehouse.WarehouseExecutor import WarehouseExecutor
from warehouse.WarehouseExecutorClient import WarehouseExecutorClient
from warehouse.ProductsExecutor import ProductsExecutor
from warehouse.ProductsExecutorClient import ProductsExecutorClient
from warehouse.BarcodeExecutor import BarcodeExecutor
from warehouse.BarcodeExecutorClient import BarcodeExecutorClient
from sales.PriceExecutor import PriceExecutor
from sales.PriceExecutorClient import PriceExecutorClient
from forecasting.DemandExecutor import DemandExecutor
from forecasting.DemandExecutorClient import DemandExecutorClient
from forecasting.DemandYearlyExecutor import DemandYearlyExecutor
from forecasting.DemandYearlyExecutorClient import DemandYearlyExecutorClient

main_loop = asyncio.get_event_loop()

#Logger.init(level=logging.DEBUG)

# Регистрируем наш слушатель - он принимает сообщения из очереди rabbitMQ

@executor(SalesOrderExecutor)
class SalesOrderExecutorListener(QueueListener):
    async def parse(self, task):
        await SalesOrderExecutor(task).parse()

@executor(BookingExecutor)
class BookingExecutorListener(QueueListener):
    async def parse(self, task):
        await BookingExecutor(task).parse()

@executor(WarehouseExecutor)
class WarehouseExecutorListener(QueueListener):
    async def parse(self, task):
        await WarehouseExecutor(task).parse()

@executor(ProductsExecutor)
class ProductsExecutorListener(QueueListener):
    async def parse(self, task):
        await ProductsExecutor(task).parse()

@executor(BarcodeExecutor)
class BarcodeExecutorListener(QueueListener):
    async def parse(self, task):
        await BarcodeExecutor(task).parse()

@executor(PriceExecutor)
class PriceExecutorListener(QueueListener):
    async def parse(self, task):
        await PriceExecutor(task).parse()

@executor(DemandExecutor)
class DemandExecutorListener(QueueListener):
    async def parse(self, task):
        await DemandExecutor(task).parse()

@executor(DemandYearlyExecutor)
class DemandYearlyExecutorListener(QueueListener):
    async def parse(self, task):
        await DemandYearlyExecutor(task).parse()



async def launch():
    sales_order_executor = SalesOrderExecutorClient(main_loop)
    order_seed = {"customer_id": 1,
                       "basket": {1:1,
                                  3:4,
                                  5:2},
                       "bill_basket": 26700,
                       "payment_method": "cod",
                       "payment_confirmed": False,
                       "shipment_type": "nextday",
                       "address_id": None,
                       "latitude": 41.358082,
                       "longitude": 69.382464,
                       "address_string": "1-1 Мунаввар-Кори, ТТЗ-2",
                       "comment": "Звонить на 264-14-18 если сотку не беру",
                       "bill_shipment": 0,
                       "bill_total": 26700,
                       "order_status": "initiated"
                       }
    print("add")
    r = await sales_order_executor.add(order_seed)
    print(r)
    print("####")
    i = await sales_order_executor.get_one(r)
    print("after")
    print(i)
    print("finish")

async def dothistime():
    executor = ProductsExecutorClient(main_loop)
    seed = {"product_id": None,
                       "name": "Спирт этиловый 70% 50мл",
                       "description": "Спирт этиловый 70% 50мл",
                       "dosage_form": "раствор",
                       "dosage_size": 50,
                       "dosage_quantity": 1,
                       "category": "Антисептики и дезинфицирующие препараты",
                       "manufacturer": "Авиценна",
                       "manufacturer_country": "Узбекистан",
                       "alternatives": None,
                       "association": None,
                       "available_next_day": True
                       }
    r = await executor.add(seed)
    print(r["product_id"])
    result = await executor.get_one({"product_id":r["product_id"]})
    print(result)

async def remove_product(pid):
    executor = ProductsExecutorClient(main_loop)
    seed = {"product_id": pid}
    await executor.remove(seed)
    print("done")

async def get_one_product(pid):
    seed = {"product_id":pid}
    executor = ProductsExecutorClient(main_loop)
    response = await executor.get_one(seed)
    print(response)

async def get_all_products():
    executor = ProductsExecutorClient(main_loop)
    all = await executor.get_all(None)
    print(all)

async def get_one_product_by_name(name):
    executor = ProductsExecutorClient(main_loop)
    seed = {"name": name}
    result = await executor.get_one_name(seed)
    print(result)

async def get_best_matches(name):
    executor = ProductsExecutorClient(main_loop)
    seed = {"name": name}
    result = await executor.best_matches(seed)
    print(result)

async def fill_some_products():
    print(0)
    executor = ProductsExecutorClient(main_loop)
    import pandas as pd
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)
    df = pd.read_csv("/var/www/vita/forecasting/drug.txt", delimiter="\t")
    c=0
    for entry in df.iterrows():
        c+=1
        seed = {"product_id": None,
                "name": entry[1]["Name"],
                "description": None,
                "dosage_form": entry[1]["Dosage_Form"],
                "dosage_size": None,
                "dosage_quantity": None,
                "category": entry[1]["Category"],
                "manufacturer": entry[1]["Manufacturer"],
                "manufacturer_country": entry[1]["Country"],
                "alternatives": None,
                "association": None,
                "available_next_day": True
                }
        r = await executor.add(seed)

        print(r["product_id"])

async def add_barcode(barcode_id, instance_id, instance_type):
    executor = BarcodeExecutorClient(main_loop)
    seed = {"barcode_id": barcode_id,
            "instance_id": instance_id,
            "instance_type": instance_type}
    instance_id = await executor.add(seed)
    print(instance_id)
    result = await executor.get_one(instance_id)
    print(result)

async def modify_barcode(barcode_id, instance_id, instance_type):
    seed = {"barcode_id": barcode_id,
            "instance_id": instance_id,
            "instance_type": instance_type}
    executor = BarcodeExecutorClient(main_loop)
    await executor.modify(seed)
    print("modified")
    await get_instance_from_barcode(barcode_id)

async def get_one_barcode(barcode_id):
    barcode_executor = BarcodeExecutorClient(main_loop)
    barcode_seed = {"barcode_id": barcode_id}
    result = await barcode_executor.get_one(barcode_seed)
    print(result)

async def get_instance_from_barcode(barcode_id):
    barcode_executor = BarcodeExecutorClient(main_loop)
    barcode_seed = {"barcode_id": barcode_id}
    barcode_executor_result = await barcode_executor.get_one(barcode_seed)
    if barcode_executor_result["instance_type"] == "product":
        products_executor = ProductsExecutorClient(main_loop)
        products_seed = {"product_id": barcode_executor_result["instance_id"]}
        products_executor_result = await products_executor.get_one(products_seed)
        print(products_executor_result)

async def generate_barcode(barcode):
    barcode_executor = BarcodeExecutorClient(main_loop)
    barcode_seed = {"barcode_id": barcode}
    await barcode_executor.generate_barcode_png(barcode_seed)

async def add_price():
    executor = PriceExecutorClient(main_loop)
    seed = {"product_id": 21,
            "price": 1000,
            "price_discounted": 900}
    instance_id = await executor.add(seed)
    result = await executor.get_one(instance_id)
    print(result)

async def modify_price():
    executor = PriceExecutorClient(main_loop)
    seed = {"product_id": 21,
            "price": 900,
            "price_discounted": 800}
    await executor.modify(seed)

async def get_price():
    executor = PriceExecutorClient(main_loop)
    seed = {"product_id": 21}
    result = await executor.get_one(seed)
    print(result)

async def remove_price():
    executor = PriceExecutorClient(main_loop)
    seed = {"product_id": 21}
    await executor.remove(seed)

async def add_demand():
    from datetime import datetime
    executor = DemandExecutorClient(main_loop)
    seed = {"product_id": 22,
            "demand_date": datetime(2018,10,5),
            "demand_forecast": 5}
    await executor.add(seed)

async def get_one_demand():
    from datetime import datetime
    executor = DemandExecutorClient(main_loop)
    seed = {"product_id": 22,
            "demand_date": datetime(2018,10,5),
            "demand_forecast": 5}

    result = await executor.get_one(seed)
    print(result)

async def modify_one_demand():
    from datetime import datetime
    executor = DemandExecutorClient(main_loop)
    seed = {"product_id": 22,
            "demand_date": datetime(2018,10,5),
            "demand_actual": 6}
    await executor.modify(seed)
    print("ok")

async def remove_one_demand():
    from datetime import datetime
    executor = DemandExecutorClient(main_loop)
    seed = {"product_id": 21,
            "demand_date": datetime(2018,10,2)}
    await executor.remove(seed)

async def dosmthconstantly():
    executor = DemandExecutorClient(main_loop)
    await executor.dosmthconstantly(None)

async def get_qty():
    products_executor = ProductsExecutorClient(main_loop)
    demand_executor = DemandYearlyExecutorClient(main_loop)

    import pandas as pd
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)
    df = pd.read_csv("/var/www/vita/forecasting/drug_qty.txt", delimiter="\t")
    c=0
    for entry in df.iterrows():
        c+=1
        seed = {"name": entry[1]["Name"]}
        r = await products_executor.get_one_name(seed)
        if len(r) > 0:
            seed = {"product_id": r["product_id"],
                    "year": 2016,
                    "quantity": entry[1]["Quantity"],
                    "jan": 0.08333,
                    "feb": 0.08333,
                    "mar": 0.08333,
                    "apr": 0.08333,
                    "may": 0.08333,
                    "jun": 0.08333,
                    "jul": 0.08333,
                    "aug": 0.08333,
                    "sep": 0.08333,
                    "oct": 0.08333,
                    "nov": 0.08333,
                    "dec": 0.08333}
            await demand_executor.add(data=seed)
            print(entry[1]["Name"], r["product_id"])

async def get_one_demand_yearly():
    executor = DemandYearlyExecutorClient(main_loop)
    seed = {"product_id": 1}

    result = await executor.get_one(data=seed)
    print(result)

async def modify_one_demand_yearly():
    executor = DemandYearlyExecutorClient(main_loop)
    seed = {"product_id": 1,
            "quantity": 400000}

    result = await executor.modify(data=seed)
    print(result)

#main_loop.create_task(modify_one_demand_yearly())
#main_loop.create_task(get_one_demand_yearly())
#main_loop.create_task(dosmthconstantly())
#main_loop.create_task(get_qty())
#main_loop.create_task(get_all_products())
#main_loop.create_task(get_one_product_by_name("Араган р-р 25мг 2,5мл"))
#main_loop.create_task(get_one_product(12249))
#main_loop.create_task(remove_product(69))
#main_loop.create_task(remove_product(62))
#main_loop.create_task(remove_product(63))
#main_loop.create_task(remove_product(64))
main_loop.create_task(dothistime())
#main_loop.create_task(add_demand())
#main_loop.create_task(modify_one_demand())
#main_loop.create_task(fill_some_products())

#main_loop.create_task(modify_barcode("812379812738","20","product"))
#main_loop.create_task(add_barcode(812379812738,None,None))

main_loop.create_task(SalesOrderExecutorListener().register_listener(main_loop))
main_loop.create_task(BookingExecutorListener().register_listener(main_loop))
main_loop.create_task(WarehouseExecutorListener().register_listener(main_loop))
main_loop.create_task(ProductsExecutorListener().register_listener(main_loop))
main_loop.create_task(BarcodeExecutorListener().register_listener(main_loop))
main_loop.create_task(PriceExecutorListener().register_listener(main_loop))
main_loop.create_task(DemandExecutorListener().register_listener(main_loop))
main_loop.create_task(DemandYearlyExecutorListener().register_listener(main_loop))
main_loop.run_forever()
