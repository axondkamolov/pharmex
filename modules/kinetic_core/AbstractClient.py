import asyncio
import json
import uuid

from aio_pika import IncomingMessage, Message

from .DateTimeDecoder import DateTimeDecoder
from .Json import json_dumps
from .MQConnector import mq


def rpc(fun):
    async def ret_rpc(self, *args, **kwargs):
        if len(kwargs) > 0:
            data = json_dumps(kwargs).encode('utf8')
        else:
            data = json_dumps(args[0]).encode('utf-8')
        result = await self.rpc_call(data, fun.__name__)
        return json.loads(result.decode("utf-8"), cls=DateTimeDecoder)

    return ret_rpc


def lpc(fun):
    async def ret_lpc(self, *args, **kwargs):
        executor = self._executor(None)
        return await getattr(executor, fun.__name__)(*args, **kwargs)

    return ret_lpc


def event(fun):
    async def ret_event(self, *args, **kwargs):
        if len(kwargs) > 0:
            data = json_dumps(kwargs).encode('utf8')
        else:
            data = json_dumps(args[0]).encode('utf-8')
        await self.event_call(data, fun.__name__)

    return ret_event


class AbstractClient:
    queue = None

    def __init__(self, loop=asyncio.get_event_loop()):
        self.connection = None
        self.channel = None
        self.callback_queue = None
        self.futures = {}
        self.loop = loop

    def on_response(self, message: IncomingMessage):
        future = self.futures.pop(message.correlation_id)
        future.set_result(message.body)

    async def _connect(self):
        self.channel = await mq.channel()
        self.callback_queue = await self.channel.declare_queue(
            exclusive=True
        )
        await self.callback_queue.consume(self.on_response)

    async def rpc_call(self, obj, fun):
        correlation_id = str(uuid.uuid4()).encode()
        future = self.loop.create_future()

        self.futures[correlation_id] = future

        if self.channel is None:
            await self._connect()

        await self.channel.default_exchange.publish(
            Message(
                obj,
                type=fun,
                content_type='text/plain',
                correlation_id=correlation_id,
                reply_to=self.callback_queue.name,
            ),
            routing_key=self.queue,
        )

        return await future

    async def event_call(self, obj, fun):
        if self.channel is None:
            await self._connect()
        await self.channel.default_exchange.publish(
            Message(
                obj,
                type=fun,
                content_type='text/plain',
            ),
            routing_key=self.queue,
        )
