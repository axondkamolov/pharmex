from .SQLify import update, insert
from .Connector import db


def table(name, key):
    class ClassWrapper:
        def __init__(self, cls):
            self.other_class = cls

        def __call__(self, *cls_ars):
            other = self.other_class(*cls_ars)
            other.table = name
            other.key = key
            return other

    return ClassWrapper


class AbstractDB:
    table = None
    key = None

    async def query(self, sql, args):
        return await db.query(sql, args)

    async def add(self, data):
        return await insert(self.table, db, data, self.key)

    async def persist(self, data, filter):
        return await update(self.table, db, data, filter, self.key)
