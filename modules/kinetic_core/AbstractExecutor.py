import inspect
import json
import logging
import urllib
from datetime import datetime

import aiobotocore
import aiofiles
from aio_pika import Message
from .Boolean import Boolean
from .DateTimeDecoder import DateTimeDecoder
from .Json import json_dumps
from .RedisConnector import *
from pathlib import Path


# def client(arg):
#     class ClassWrapper:
#         def __init__(self, cls):
#             self.other_class = cls
#
#         def __call__(self, *cls_ars):
#             other = self.other_class(*cls_ars)
#             n = arg.__name__
#             if n.find(".") > 0:
#                 other.queue = n[len(arg.__package__) + 1:]
#             else:
#                 other.queue = n
#             return other
#
#     return ClassWrapper

def executor(arg):
    class ClassWrapper:
        def __init__(self, cls):
            self.other_class = cls

        def __call__(self, *cls_ars):
            other = self.other_class(*cls_ars)
            n = arg.__name__
            if n.find(".") > 0:
                other.queue = n[len(arg.__package__) + 1:]
            else:
                other.queue = n
            other._executor = arg
            return other

    return ClassWrapper


def verify(fun):
    async def ret_fun(self, *args, **kwargs):
        argument = args[0]
        data = await self.get(argument[self.uid])
        if data is None:
            await self.error("TRIP_DOESNT_EXISTS")
            return
        if not fun(*args, **kwargs):
            await self.sync(data)  # print("wrong capacity modification")
        else:
            await self.save(data)

    return ret_fun


class AbstractExecutor:

    def __init__(self, task):
        self.queue = self.__class__.__name__
        if task is not None:
            self.exchange = task[0]
            self.message = task[1]
            self.published = False
            body = self.message.body
            if len(body) > 0:
                if isinstance(body, str):
                    self.data = json.loads(body, cls=DateTimeDecoder)
                else:
                    self.data = json.loads(body.decode("utf-8"), cls=DateTimeDecoder)
        self.uid = None

    async def _save_file(self, path):
        filename = str(datetime.now()) + Path(path).name
        key = '{}/{}'.format(config.aws_bucket_folder, filename)

        session = aiobotocore.get_session(loop=loop)
        async with session.create_client('s3', region_name='eu-west-1',
                                         aws_secret_access_key=config.aws_secret_access_key,
                                         aws_access_key_id=config.aws_access_key_id) as aws_client:
            # upload object to amazon s3
            async with aiofiles.open(path, mode='rb') as f:
                data = await f.read()
                await aws_client.put_object(Bucket=config.aws_bucket,
                                            Key=key,
                                            Body=data)
                await aws_client.put_object_acl(ACL='public-read',
                                                Bucket=config.aws_bucket,
                                                Key=key)
                return {"url": "https://s3-eu-west-1.amazonaws.com/"+urllib.parse.quote(config.aws_bucket+"/"+key),
                        "key": key}

    async def _remove_file(self, key):

        session = aiobotocore.get_session(loop=loop)
        async with session.create_client('s3', region_name='eu-west-1',
                                         aws_secret_access_key=config.aws_secret_access_key,
                                         aws_access_key_id=config.aws_access_key_id) as aws_client:
            # get object from s3
            resp = await aws_client.delete_object(Bucket=config.aws_bucket, Key=key)
            return resp

    # save each value as key { value=value, timestamp=timestamp)
    async def save(self, d, parent_key=None):
        if parent_key is None:
            parent_key = ""
        for key, value in d.items():
            if parent_key == "":
                local_key = str(key)
            else:
                local_key = str(parent_key) + ":" + str(key)
            if type(value) is dict:
                for key2, value2 in value.items():
                    inner_key = local_key + ":" + str(key2)
                    if type(value2) is dict or type(value2) is list:
                        await self.save({key2: value2}, parent_key=local_key)
                    else:
                        if value2 != None:
                            await self._put(inner_key, value2)
                        else:
                            await self._rm(inner_key)
            elif type(value) is list:
                for key2, value2 in enumerate(value):
                    inner_key = local_key + ":" + str(key2)
                    if type(value2) is dict:
                        await self.save({key2: value2}, parent_key=local_key)
                    else:
                        if value2 != None:
                            await self._put(inner_key, value2)
                        else:
                            await self._rm(inner_key)
            else:
                if value != None:
                    await self._put(local_key, value)
                else:
                    await self._rm(local_key)

    async def _rm(self, key):
        cnt = 10000
        crs = 0
        z = list()
        key = key + "*"
        crs, tt = (await redis_pool.hscan(self.queue, match=key, count=cnt, cursor=crs))
        z += tt
        while crs != 0:
            crs, tt = (await redis_pool.hscan(self.queue, match=key, count=cnt, cursor=crs))
            z += tt
        for t in z:
            await redis_pool.hdel(self.queue, t[0])

    async def _put(self, key, value):
        str_timestamp = (await redis_pool.hmget(self.queue, key + ":r_timestamp"))[0]

        if str_timestamp is None:
            current_timestamp = 0
        else:
            current_timestamp = int(str_timestamp)
        has = hasattr(value, 'r_timestamp') and type(value) is not datetime
        v = value.value if has else value
        if type(v).__name__ == "tmp":
            v = v.value
        t = type(v).__name__
        if type(v) is bool:
            if v:
                v = 1
            else:
                v = 0
        if type(v) is Boolean:
            v = v.value
        if type(v) is datetime:
            v = v.timestamp()
        if current_timestamp == 0 or (has and current_timestamp < value.r_timestamp):
            await redis_pool.hmset(self.queue, key + ":value", v)
            await redis_pool.hmset(self.queue, key + ":r_timestamp", value.r_timestamp if has else 0)
            await redis_pool.hmset(self.queue, key + ":type", t)
        else:
            logging.error("old value for key " + key)

    async def get(self, *key):
        result_key = ""
        for k in key:
            if type(k) is list:
                for kk in k:
                    if result_key != "":
                        result_key += ":"
                    result_key += str(kk)
            else:
                if result_key != "":
                    result_key += ":"
                result_key += str(k[0])

        if result_key.find(":") < 0:
            result_key += ":"
        result_key += "*"
        cnt = 10000
        crs = 0
        z = list()

        crs, tt = (await redis_pool.hscan(self.queue, match=result_key, count=cnt, cursor=crs))
        z += tt
        while crs != 0:
            crs, tt = (await redis_pool.hscan(self.queue, match=result_key, count=cnt, cursor=crs))
            z += tt
        return get_one(z, *key)

    async def _list(self):
        response = await redis_pool.hgetall(self.queue)
        return get_one(response)

    async def delete(self, trip):
        if trip is not None:
            await redis_pool.hdel(self.queue, trip[self.uid])

    async def sync(self, data):
        pass

    async def error(self, code):
        # send error back to originator
        pass

    async def fail(self, message, trip):
        # save error message in redis
        await self.sync(trip)

    async def parse(self):
        attrs = inspect.getfullargspec(getattr(self, str(self.message.type)))
        if len(attrs.args) > 1:
            result = await getattr(self, str(self.message.type))(**self.data)
        else:
            result = await getattr(self, str(self.message.type))()
        if not self.published and self.message.reply_to is not None:
            await self.publish(result)

    async def publish(self, response):
        self.published = True
        await self.exchange.publish(
            Message(
                body=json_dumps(response).encode("utf-8"),
                correlation_id=self.message.correlation_id
            ),
            routing_key=self.message.reply_to
        )
