from .AbstractClient import AbstractClient, event


class AbstractMobileClient(AbstractClient):

    @event
    def ack(self):
        pass
