import asyncio, json, time

from .AbstractExecutor import AbstractExecutor
from .RedisConnector import redis_pool


class AbstractMobileExecutor(AbstractExecutor):

    def __init__(self, task, webSocketConnector):
        super().__init__(task)
        self.webSocketConnector = webSocketConnector

    async def _send(self, uid, data):
        await self.webSocketConnector.send(uid, data)

    async def _send_important(self, uid, data):
        data['pid'] = int(time.time() * 1000.0)
        await self.webSocketConnector.send(uid, data)
        await redis_pool.hmset(self.queue + str('_firebase'), str(data['pid']) + '#' + str(data['uid']), json.dumps(self.data))
        await asyncio.sleep(3)
        a = await (redis_pool.hmget(self.queue + str('_firebase'), str(data['pid']) + '#' + str(data['uid'])))
        if a is None or a[0] is None:
            pass
        else:
            await redis_pool.hdel(self.queue + str('_firebase'), str(data['pid']) + '#' + str(data['uid']))
            print('push to firebase')

    async def ack(self):
        await redis_pool.hdel(self.queue + str('_firebase'), str(self.data['pid']) + '#' + str(self.data['uid']))