import asyncio
import uuid
from abc import abstractmethod, ABC
import logging

from cyrusbus import Bus
from autobahn.asyncio.websocket import WebSocketServerProtocol

bus = Bus()


class AbstractWebSocketProtocol(WebSocketServerProtocol, ABC):

    def __init__(self):
        super().__init__()
        self.uid = None
        self.__connection_id = str(uuid.uuid4())

    def onConnect(self, request):
        logging.debug("Client connecting: {0}".format(request.peer))
        if "token" in request.params:
            # verify token
            self.uid = 10 #request.params["token"][0]

    def onOpen(self):
        if self.uid is None:
            self.sendClose(1000, "Wrong authorization token")
            logging.warning("Wrong authorization token")
        else:
            try:
                bus.publish("websocket.open", self.uid, self)
            except Exception as e:
                logging.error(e)

    def onClose(self, was_clean, code, reason):
        bus.publish("websocket.close", self.uid, self)

    def onMessage(self, payload, isBinary):
        if not isBinary:
            asyncio.get_event_loop().create_task(self.onPacket(payload.decode('utf8')))

    @abstractmethod
    def onPacket(self, text):
        pass
