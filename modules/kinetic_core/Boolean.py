class Boolean:
    value = 0

    def __init__(self, b):
        if type(b) is Boolean:
            self.value = b.value
        else:
            if b:
                self.value = 1
            else:
                self.value = 0

    def __repr__(self):
        if self.value == 1:
            return 'True'
        else:
            return 'False'

    def __bool__(self):
        return self.value == 1
