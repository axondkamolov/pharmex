from datetime import timezone

import asyncpg
import logging

from .Boolean import Boolean
from .Null import Null
from .DateTime import DateTime
from .DuckPunching import attr
from .config import config


class Database:

    def __init__(self):
        self.pool = None

    async def query(self, sql, args=None):
        data = tuple()
        if args is not None:
            for k, v in enumerate(args):

                if hasattr(v, "r_timestamp") and hasattr(v, "value"):
                    if v.value == None:
                        v = None
                    elif isinstance(v, Boolean):
                        v = v.value.value == 1
                    else:
                        v = v.value
                elif isinstance(v, Boolean):
                        v = v.value == 1
                data = (*data, v)

        try:
            if self.pool is None:
                self.pool = await asyncpg.create_pool(user=config.db_user, password=config.db_password,
                                                      min_size=1, max_size=1,
                                                      database=config.db, host=config.db_host, port=config.db_port)
            async with self.pool.acquire() as connection:
                async with connection.transaction():
                    print("fetch")
                    if args is None:
                        r = await connection.fetchrow(sql)
                    else:
                        r = await connection.fetchrow(sql, *data)
                    if r is not None:
                        timestamp = int(r["current_timestamp"].replace(tzinfo=timezone.utc).timestamp())
                        d = {}
                        timestamp = DateTime(timestamp)
                        for key, value in r.items():
                            if key != "current_timestamp":
                                v = value if value is not None else Null()
                                d[key] = attr(v, 'timestamp', timestamp)

                        return d
                    return r
        except Exception as e:
            logging.error(e)
            print(e)
            self.pool = await asyncpg.create_pool(user=config.db_user, password=config.db_password,
                                                  min_size=1, max_size=1,
                                                  database=config.db, host=config.db_host, port=config.db_port)
            with self.pool.acquire() as connection:
                async with connection.transaction():
                    if args is None:
                        r = await connection.fetchrow(sql)
                    else:
                        r = await connection.fetchrow(sql, *data)

                    await connection.commit()
                    if r is not None:
                        timestamp = int(r["current_timestamp"].replace(tzinfo=timezone.utc).timestamp())
                        d = {}
                        timestamp = DateTime(timestamp)
                        for key, value in r.items():
                            if key != "current_timestamp":
                                v = value if value is not None else Null()
                                d[key] = attr(v, 'timestamp', timestamp)

                        return d
                    return r


db = Database()
