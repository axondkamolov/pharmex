from datetime import datetime


class DateTime:

    def __init__(self, value):
        if type(value) is DateTime:
            self.date = value.date
        else:
            self.date = datetime.fromtimestamp(int(value))
