from datetime import datetime
from json import JSONEncoder

from .DateTime import DateTime
from .Boolean import Boolean


class DateTimeEncoderCompact(JSONEncoder):
    """ Instead of letting the default encoder convert datetime to string,
        convert datetime objects into a dict, which can be decoded by the
        DateTimeDecoder
    """

    def default(self, obj):
        if isinstance(obj, datetime):
            return {
                '__type__': 'datetime',
                'year': obj.year,
                'month': obj.month,
                'day': obj.day,
                'hour': obj.hour,
                'minute': obj.minute,
                'second': obj.second,
                'microsecond': obj.microsecond,
            }
        elif hasattr(obj, "r_timestamp") and hasattr(obj, "value"):
            if isinstance(obj.value, DateTime):
                v = obj.value.date
                return {
                    '__type__': 'datetime',
                    'year': v.year,
                    'month': v.month,
                    'day': v.day,
                    'hour': v.hour,
                    'minute': v.minute,
                    'second': v.second,
                    'microsecond': v.microsecond,
                }
            elif obj.value == None:
                return None
            elif isinstance(obj, Boolean):
                return obj.value.value == 1
            return obj.value
        elif obj.value == None:
            return None
        elif isinstance(obj, Boolean):
            return obj.value.value == 1
        else:
            return JSONEncoder.default(self, obj)
