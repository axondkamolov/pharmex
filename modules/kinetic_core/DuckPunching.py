from .Null import Null


def attr(e, n, v):  # will work for any object you feed it, but only that object
    class tmp(type(e)):
        def attr(self, n, v):
            setattr(self, n, v)
            self.value = e
            return self

        def __str__(self):
            print(self.value)
            if type(self.value) is Null:
                return str(None)
            return str(self.value)

        def __repr__(self):
            if type(self.value) is Null:
                return str(None)
            return str(self.value)

    return tmp(e).attr(n, v)
