import json

from .DateTimeEncoder import DateTimeEncoderCompact


def json_dumps(data):
    return json.dumps(data, cls=DateTimeEncoderCompact)
