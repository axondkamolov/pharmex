import logging
import logging.handlers


def init(level):
    # set logging level for rabbitmq
    logging.getLogger("aio_pika").setLevel(logging.WARN)
    filename = '/tmp/myservice.log'
    handler2 = logging.handlers.TimedRotatingFileHandler(filename, when="midnight", backupCount=2,
                                                         encoding="utf-8")

    #rabbit = RabbitMQHandler(host='127.0.0.1', port=config.rabbit_port, username="test",
    #                         password="test", exchange="")
    #rabbit.setLevel(logging.ERROR)

    # logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=level)
    logging.getLogger().addHandler(handler2)




