import asyncio
import aio_pika
from .config import config
loop = asyncio.get_event_loop()


class RabbitMQConnector:

    def __init__(self, loop):
        self.loop = loop
        self.connection = None

    async def channel(self):
        if self.connection is None:
            self.connection = await aio_pika.connect_robust(
                "amqp://test:test@" + config.rabbit_host
                + ":" + str(config.rabbit_port) + "/", loop=self.loop, port=config.rabbit_port
            )
        return await self.connection.channel()


mq = RabbitMQConnector(loop)
