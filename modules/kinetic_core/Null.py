class Null:
    value = None

    def __init__(self, b=False):
        pass

    def __eq__(self, *args, **kwargs):
        return args[0] is None

    def __str__(self):
        return str(None)
