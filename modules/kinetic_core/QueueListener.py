from abc import ABC, abstractmethod
from functools import partial

import aio_pika
from aio_pika import Exchange, IncomingMessage

from .config import config


class QueueListener(ABC):
    queue = None

    async def register_listener(self, loop):
        connection = await aio_pika.connect_robust("amqp://test:test@" + config.rabbit_host
                                                   + ":" + str(config.rabbit_port) + "/",
                                                   loop=loop)
        channel = await connection.channel()
        queue = await channel.declare_queue(self.queue, auto_delete=False, durable=True)

        await queue.consume(
            partial(
                self._parse,
                channel.default_exchange
            )
        )

    async def _parse(self, exchange: Exchange, message: IncomingMessage):
        with message.process():
            await self.parse((exchange, message))

    @abstractmethod
    async def parse(self, task):
        pass
