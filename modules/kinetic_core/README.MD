##### Проброс порта
```bash
ssh -L 35672:localhost:15672 -L 3335:localhost:5672 -L 3334:localhost:6379 -L 3334:vita.citioj9smjkf.eu-west-1.rds.amazonaws.com:6379  -i gotaxiapp.pem ubuntu@ec2-34-245-223-245.eu-west-1.compute.amazonaws.com
```
##### Универсальный модуль с базовыми классами:
```bash
git submodule add git@bitbucket.org:anton_vasiljev/kinetic-core.git modules/kinetic_core
```

##### Обновление репозитариев
```bash
git pull --recurse-submodules 
```

##### Обновление репозитариев - если пред. не работает
```bash
git submodule update --init --force --remote
```

##### Установка зависимостей
```bash
pip install -r modules/kinetic_core/requirements.txt
```


##### Пример регистрации executor:
```

# Регистрируем наш слушатель - он принимает сообщения из очереди rabbitMQ
@executor(SomeExecutor)
class TestQueueListener(QueueListener):

    async def parse(self, task):
        await SomeExecutor(task).parse()
        

main_loop = asyncio.get_event_loop()
main_loop.create_task(TestQueueListener().register_listener(main_loop))
main_loop.create_task(fill_some_products())
main_loop.run_forever()
```
##### Пример интерфейса:
```
@executor(TestExecutor)
class TestClient(AbstractClient):

    @rpc
    async def add(self, data=None):
        pass
        
    @lpc
    async def list(self):
        pass
        
    @event
    async def notify(self, message):
        pass
```
##### Пример обработчика:
```
@executor(TestExecutor)
class TestClient(AbstractClient):

    async def add(self, data=None):
        # do something on remote machine and return result
        pass
        
    async def list(self):
        # do something in same loop on client machine and return result
        pass
    
    async def notify(self, message=None):
        # this function does not return a response
        
```
