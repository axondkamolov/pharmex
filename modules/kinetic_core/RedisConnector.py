import asyncio

import aioredis

from .DateTime import DateTime
from .Boolean import Boolean
from .DuckPunching import attr
from .Util import merge
from .config import config

loop = asyncio.get_event_loop()


async def pool():
    url = 'redis://' + str(config.redis_host) + ':' + str(config.redis_port)
    return await aioredis.create_redis_pool(
        url,
        password="2cd6249b0db91d1b3ed5a9b939b8713d182f9a99846ae321d5138a46385956c8",
        minsize=5, maxsize=10,
        encoding='utf-8',
        loop=loop)


redis_pool = loop.run_until_complete(pool())


def get_one(d, *key):
    l = len(key)
    if type(key) is tuple:
        if len(key) > 0 and type(key[0] is list):
            l = len(key[0])
    result = {}
    if d is None:
        return None
    if isinstance(d, dict):
        it = d.items()
    else:
        it = d
    for key, value in it:
        keys = key.split(":")
        if keys[-1] == "r_timestamp" or keys[-1] == "type":
            continue
        timestamp = 0
        t = None
        for k, v in it:
            if k == key[0: key.rfind(":")] + ":r_timestamp":
                timestamp = v
            if k == key[0: key.rfind(":")] + ":type":
                t = v

        if t == "datetime":
            value = DateTime(float(value))
        elif t == "float":
            value = float(value)
        elif t == "int":
            value = int(value)
        elif t == "bool":
            value = Boolean(b=(value == "1"))

        value = attr(value, "r_timestamp", timestamp)
        cur_dict = {keys[-2]: value}
        if len(keys) - l > 1:
            for x in range(len(keys) - 3, l - 1, -1):
                i_key = keys[x]
                t_dict = {i_key: cur_dict}
                cur_dict = t_dict
            result = merge(cur_dict, result)
        else:
            return value

    return result
