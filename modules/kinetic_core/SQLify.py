from .Json import json_dumps


def read(table, **kwargs):
    """ Generates SQL for a SELECT statement matching the kwargs passed. """
    sql = list()
    sql.append("SELECT * FROM %s " % table)
    if kwargs:
        sql.append("WHERE " + " AND ".join("%s = '%s'" % (k, v) for k, v in kwargs.iteritems()))
    sql.append(";")
    return "".join(sql)


async def upsert(table, db, data):
    """ update/insert rows into objects table (update if the row already exists)
        given the key-value pairs in kwargs """
    keys = data.keys()
    values = data.values()
    sql = list()
    sql.append("INSERT INTO %s (" % table)
    sql.append(", ".join(keys))
    sql.append(") VALUES (")
    sql.append(", ".join(map(lambda x: str(x) if x is not None else "null", values)))
    sql.append(") ON DUPLICATE KEY UPDATE ")
    sql.append(", ".join("%s = '%s'" % (k, v) for k, v in data.items()))
    sql.append(" returning ")

    sql.append(", ".join(k for k, v in data.items()))
    sql.append(" current_timestamp;")
    print("".join(sql))
    return await db.query("".join(sql))


async def insert(table, db, data, key):
    """ update/insert rows into objects table (update if the row already exists)
        given the key-value pairs in kwargs """
    if key in data:
        del data[key]
    keys = data.keys()
    values = data.values()
    vals = tuple()
    for v in values:
        if type(v) is dict or type(v) is tuple or type(v) is list:
            vals = (*vals, json_dumps(v))
        else:
            vals = (*vals, v)
    sql = list()
    sql.append("INSERT INTO %s (" % table)
    sql.append(", ".join(keys))
    sql.append(") VALUES (")
    sql.append(", ".join("$" + str(x + 1) for x in range(len(values))))
    sql.append(") returning ")

    sql.append(", ".join(k for k, v in data.items()))
    sql.append(", " + key)
    sql.append(", current_timestamp;")
    print("".join(sql))
    return await db.query("".join(sql), vals)


async def update(table, db, data, filter, key):
    """ update/insert rows into objects table (update if the row already exists)
        given the key-value pairs in kwargs """
    sql = list()
    sql.append("UPDATE  %s" % table)
    sql.append(" SET ")
    vs = dict()
    l = len(data.keys())
    for k, v in enumerate(data.keys()):
        vs[v] = "$" + str(k + 1)
    sql.append(", ".join("%s = %s" % (k, v) for k, v in vs.items()))
    sql.append(" WHERE ")
    sql.append(key)
    sql.append("=")
    sql.append("$"+str(l + 1))
    sql.append(" returning ")

    sql.append(", ".join(k for k, v in data.items()))
    sql.append(", current_timestamp;")

    print("".join(sql))
    vals = tuple()
    for v in data.values():
        if type(v) is dict or type(v) is tuple or type(v) is list:
            vals = (*vals, json_dumps(v))
        else:
            vals = (*vals, v)
    vals = (*vals, filter)
    return await db.query("".join(sql), vals)


def delete(table, **kwargs):
    """ deletes rows from table where **kwargs match """
    sql = list()
    sql.append("DELETE FROM %s " % table)
    sql.append("WHERE " + " AND ".join("%s = '%s'" % (k, v) for k, v in kwargs.iteritems()))
    sql.append(";")
    return "".join(sql)
