from modules.kinetic_core.QueueListener import QueueListener


class SimpleQueueListener(QueueListener):

    def __init__(self, cl):
        self.cl = cl

    async def parse(self, task):
        await self.cl(task).parse()
