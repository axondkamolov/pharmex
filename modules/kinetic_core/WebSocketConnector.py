import json
import logging

from autobahn.asyncio import WebSocketServerFactory

from modules.kinetic_core.DateTimeEncoder import DateTimeEncoderCompact
from modules.kinetic_core.AbstractWebSocketProtocol import bus


class WebSocketConnector:
    connections = {}

    def connect(self, loop, protocol):
        factory = WebSocketServerFactory("ws://127.0.0.1:9001")
        factory.protocol = protocol
        coro = loop.create_server(factory, host='0.0.0.0', port=9001)
        server = loop.run_until_complete(coro)

        logging.debug('WebSocket serving on {}'.format(server.sockets[0].getsockname()))

        bus.subscribe("websocket.open", lambda bus, uid, connection: self.open(uid, connection))
        bus.subscribe("websocket.close", lambda bus, uid, connection: self.close(uid))

    def open(self, uid, connection):
        self.connections[uid] = connection

    def close(self, uid):
        self.connections.pop(uid, None)

    def send(self, uid, data):
        if uid in self.connections:
            self.connections[uid]\
                .sendMessage(json.dumps(data, cls=DateTimeEncoderCompact,
                                        separators=(',', ':'), ensure_ascii=False).encode("utf8"))
        else:
            pass
            #send via firebase
