from modules.kinetic_core.AbstractClient import *


class FirebaseClient(AbstractClient):

    @event
    async def push(self, data):
        """
        :param data: dict with uid and data,
        :return:
        """
        pass

    @event
    async def broadcast(self, data):
        """
        :param data: dict with uid and data,
        :return:
        """
        pass

