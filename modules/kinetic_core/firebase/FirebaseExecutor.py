from modules.kinetic_core.firebase.FirebaseClient import FirebaseClient
from modules.kinetic_core.AbstractExecutor import AbstractExecutor, client


@client(FirebaseClient)
class FirebaseExecutor(AbstractExecutor):

    def __init__(self, task, firebase):
        super().__init__(task)
        self.firebase = firebase

    async def push(self):
        await self.firebase.push(self.data["uid"], self.data["data"])

    async def broadcast(self):
        await self.firebase.broadcast(self.data["topic"], self.data["data"])
