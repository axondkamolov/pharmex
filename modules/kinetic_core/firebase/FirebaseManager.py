import asyncio

import pyrebase
from pyfcm import FCMNotification

from modules.kinetic_core.DateTimeEncoder import DateTimeEncoderCompact


class FirebaseManager:

    def __init__(self, config):
        self.config = config
        #self.firebase = pyrebase.initialize_app(config)

    async def auth(self, uid):
        loop = asyncio.get_event_loop()
        result = await loop.create_task(self._auth(uid))
        return {"result": result}  # todo: return useful result later

    async def _auth(self, driver_id):
        auth = self.firebase.auth()
        return auth.create_custom_token(str(driver_id))

    async def broadcast(self, topic, data, time_to_live: int = None):
        loop = asyncio.get_event_loop()
        result = await loop.create_task(self._broadcast(topic, data, time_to_live))
        return {"result": result}  # todo: return useful result later

    async def _broadcast(self, topic, data, time_to_live: int = None):
        push_service = FCMNotification(
            api_key=self.config['fcm_key'],
            json_encoder=DateTimeEncoderCompact
        )
        return push_service.notify_topic_subscribers(
            topic_name=topic,
            time_to_live=time_to_live,
            data_message=data,
        )

    async def push(self, uid, data=None, time_to_live: int = None):
        loop = asyncio.get_event_loop()
        result = await loop.create_task(self._push(uid, data, time_to_live))
        return {"result": result}  # todo: return useful result later

    async def _push(self, uid, data=None, time_to_live: int = None):
        push_service = FCMNotification(
            api_key=self.config['fcm_key'],
            json_encoder=DateTimeEncoderCompact
        )
        return push_service.single_device_data_message(
            registration_id=uid,
            time_to_live=time_to_live,
            data_message=data,
        )
