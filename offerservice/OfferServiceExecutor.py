import sys

# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')

from modules.kinetic_core.AbstractExecutor import *
from agents import AgentExecutorClient

class OfferServiceExecutor(AbstractExecutor):
    def __init__(self, task):
        super().__init__(task)

    async def offer(self, data):
        """Generate optimized prices and save to DB"""
        self.data = {}

        agent_executor = AgentExecutorClient()

        basket = data['basket']
        optimized_prices = data['optimized_prices']

        self.data['offers'] = await self.get_offers(basket=basket, optimized_prices=optimized_prices)

        for sid in self.data['offers'].keys():
            print("Sending offer to supplier #{}".format(sid))
            self.data['offers'][sid]['created_at'] = datetime.now()
            # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!VERY IMPORTANT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            await agent_executor.send_offer(sid, self)

        return self.data

    async def get_qty(self, pid, basket):
        for p in basket:
            if p['pid'] == pid:
                return p['qty']

        return 0

    async def update_timer(self, sid, ):
        pass

    async def get_offers(self, basket, optimized_prices):
        offers = {}
        for i in optimized_prices:
            if i['sid'] not in offers.keys():
                offers[i['sid']] = {"products": []}
                offers[i['sid']]['products'].append(
                    {'pid': i['pid'], 'qty': await self.get_qty(i['pid'], basket), 'exp': i['exp'], 'price': i['price']})

        return offers
