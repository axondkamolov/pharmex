import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')

from modules.kinetic_core.AbstractClient import *
from modules.kinetic_core.AbstractExecutor import executor
from offerservice.OfferServiceExecutor import OfferServiceExecutor

@executor(OfferServiceExecutor)
class OfferServiceExecutorClient(AbstractClient):

    @rpc
    async def offer(self, data):
        pass