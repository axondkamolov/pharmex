from modules.kinetic_core.AbstractClient import *
from modules.kinetic_core.AbstractExecutor import executor
from operations.TaskExecutor import TaskExecutor


@executor(TaskExecutor)
class TaskExecutorClient(AbstractClient):

    @rpc
    async def add(self, data):
        """
        Adds instance to database and redis
        :param data: Dict
        :return: Instance Id
        """
        pass

    @rpc
    def remove(self, data):
        """
        Removes instance from redis and (marks as completed in DB / removes from DB)
        :param data: Dict
        :return: None
        """
        pass

    @rpc
    def get_one(self, data):
        """
        Get instance from redis and
        :param data: Dict
        :return: Dict
        """
        pass

    @rpc
    def modify(self, data):
        """
        modifies the instance
        :param data: Empty
        :return: None
        """
        pass

    @rpc
    def get_all(self, data):
        """
        gets all instances
        :param data: Empty
        :return: None
        """
        pass

    @rpc
    def get_active(self, data):
        """
        gets some instances
        :param data: Empty
        :return: None
        """
        pass

