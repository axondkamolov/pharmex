from modules.kinetic_core.Connector import db
import json

async def db_add(data):
    payload = json.dumps(data["payload"])
    return (await db.query(
        'insert into task(destination, destination_id, command, payload, completed) values ($1, $2, $3, $4, $5) returning task_id, current_timestamp',
        (data["destination"], data["destination_id"], data["command"], payload, data["completed"])))

async def db_remove(instance_id):
    return (await db.query('delete from task where task_id=$1 returning task_id, current_timestamp',
                           (instance_id)))

async def db_modify(data):
    payload = json.dumps(data["payload"])
    return (await db.query('update task set destination=$1, destination_id=$2, command=$3, payload=$4, completed=$5 where task_id=$6 returning destination, destination_id, command, payload, completed, current_timestamp',
        ((data["destination"], data["destination_id"], data["command"], payload, data["completed"], data["task_id"]))))

