import asyncio
import logging
import pandas as pd

from aio_pika import Exchange, IncomingMessage

from modules.kinetic_core import Logger
from modules.kinetic_core.AbstractExecutor import executor
from modules.kinetic_core.QueueListener import QueueListener
from operations.TaskExecutor import TaskExecutor
from operations.TaskExecutorClient import TaskExecutorClient

main_loop = asyncio.get_event_loop()

#Logger.init(level=logging.DEBUG)

# Регистрируем наш слушатель - он принимает сообщения из очереди rabbitMQ

@executor(TaskExecutor)
class TaskExecutorListener(QueueListener):
    async def parse(self, task):
        await TaskExecutor(task).parse()

async def launch():
    instance_id = await add()
    #await modify(instance_id)
    await get_one(instance_id)
    await get_all()
    await remove(instance_id)
    await get_one(instance_id)

async def add():
    executor = TaskExecutorClient(main_loop)
    seed = {"destination": "manager",
            "destination_id": 1,
            "command": "command2",
            "payload": {"var1": 1,
                        "var2": 2}
            }
    instance_id = await executor.add(seed)
    return instance_id["task_id"]

async def modify(instance_id):
    seed = {"task_id": instance_id,
            "command": "command2",
            }
    executor = TaskExecutorClient(main_loop)
    await executor.modify(seed)
    print("modified")

async def get_one(instance_id):
    executor = TaskExecutorClient(main_loop)
    seed = {"task_id": instance_id}
    result = await executor.get_one(seed)
    print(result)

async def get_all():
    executor = TaskExecutorClient(main_loop)
    result = await executor.get_all(None)
    print(result)

async def get_active():
    executor = TaskExecutorClient(main_loop)
    result = await executor.get_active(None)
    print(result)

async def remove(instance_id):
    executor = TaskExecutorClient(main_loop)
    seed = {"task_id": instance_id}
    await executor.remove(seed)
    print("removed")

main_loop.create_task(launch())

main_loop.create_task(TaskExecutorListener().register_listener(main_loop))
main_loop.run_forever()
