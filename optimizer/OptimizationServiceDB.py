import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')
from modules.kinetic_core.Connector import db
import json

async def db_add_optimized_price_ranges(data):
    return (await db.query(
        'insert into optimized_price_ranges(filtered_list_id, optimized_price_ranges, created_at, confirmed) values '
        '($1, $2, $3, $4) returning optimized_price_ranges_id, current_timestamp',
        (data['filtered_list_id'], json.dumps(data["optimized_price_ranges"]), data['created_at'], data['confirmed'])
    ))

async def db_add_optimized_prices(data):
    return (await db.query(
        'insert into optimized_prices(optimized_price_ranges_id, optimized_prices, created_at, confirmed) values '
        '($1, $2, $3, $4) returning optimized_prices_id, current_timestamp',
        (data['optimized_price_ranges_id'], json.dumps(data["optimized_prices"]), data['created_at'], data['confirmed'])
    ))