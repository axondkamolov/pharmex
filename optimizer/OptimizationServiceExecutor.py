import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')

from modules.kinetic_core.AbstractExecutor import *
from optimizer.OptimizationServiceDB import *

class OptimizationServiceExecutor(AbstractExecutor):
    def __init__(self, task):
        super().__init__(task)

    async def get_optimized_prices(self, data, flag=False):
        """Generate optimized prices and save to DB"""
        filtered_list = data
        return await self.generate_optimized_prices(filtered_list, flag)

    async def generate_optimized_prices(self, filtered_list, flag=False):
        optimized_prices = {
            1: {102: 25},
            21: {114: 25},
            33: {101: 100}
            # sid: {pid: percent}
        }
        if flag:
            optimized_prices = {
                1: {102: 50},
                21: {114: 50},
                33: {101: 75}
                # sid: {pid: percent}
            }
        # [
        #     {"sid": 1, "pid": 102, "price": 2500, "exp": datetime.strptime("2019-05", "%Y-%m")},
        #     {"sid": 21, "pid": 114, "price": 10000, "exp": datetime.strptime("2019-05", "%Y-%m")},
        #     {"sid": 33, "pid": 101, "price": 1000, "exp": datetime.strptime("2019-05", "%Y-%m")}
        # ]

        return optimized_prices