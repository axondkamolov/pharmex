import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')

from modules.kinetic_core.AbstractClient import *
from modules.kinetic_core.AbstractExecutor import executor
from optimizer.OptimizationServiceExecutor import OptimizationServiceExecutor

@executor(OptimizationServiceExecutor)
class OptimizationServiceExecutorClient(AbstractClient):

    @rpc
    async def get_optimized_price_ranges(self, data):
        pass

    @rpc
    async def get_optimized_prices(self, data):
        pass