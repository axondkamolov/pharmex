import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')
from modules.kinetic_core.AbstractExecutor import *
from filterservice.FilterServiceExecutorDB import *
from pricelist.PriceListExecutorClient import PriceListExecutorClient
from filterservice.FilterServiceExecutorClient import FilterServiceExecutorClient
from optimizer.OptimizationServiceExecutorClient import OptimizationServiceExecutorClient
from orderingservice.OrderingServiceExecutorDB import *
from preorderingservice.PreorderingServiceExecutor import get_cost, remove_used
from datetime import timedelta

class OrderingServiceExecutor(AbstractExecutor):
    def __init__(self, task):
        super().__init__(task)

    async def order(self, data):
        self.data = {}
        self.data['offers'] = {}
        price_list_executor = PriceListExecutorClient()
        filter_service_executor = FilterServiceExecutorClient()
        optimization_service_executor = OptimizationServiceExecutorClient()

        self.data['basket'] = data['basket']
        self.data['basket_id'] = (await db_add_basket(data))["basket_id"]
        await self.save({'o' + str(self.data['basket_id']): data})

        print("Processing the order...")

        """Getting pricelist from pricelists holder"""
        self.data['basket_pricelist'] = await price_list_executor.get_basket_pricelist(data=data['basket'])

        """Filtering pricelist by expiry, rating, etc."""
        self.data['filtered_pricelist'] = await filter_service_executor.get_filtered_list(data={'basket_pricelist': self.data['basket_pricelist'], 'basket': data})

        print("Optimal price selection...")

        """Choosing optimal price range for each product"""
        self.data['optimized_prices'] = await optimization_service_executor.get_optimized_prices(data=self.data['filtered_pricelist'][data['prepayment_percent']])

        self.data['total_cost'] = get_cost(filtered_pricelist=self.data['filtered_pricelist'], optimized_prices=self.data['optimized_prices'], basket=self.data['basket'])

        if self.data['total_cost'] > data['max_price']:
            """Notify"""
            pass

        """TRANSACTION"""
        for supplier_id in self.data['optimized_prices'].keys():
            offer = {}
            for price_version in self.data['optimized_prices'][supplier_id].keys():
                offer[price_version] = {}
                for product_id in self.data['optimized_prices'][supplier_id][price_version]:
                    product = self.data['filtered_pricelist'][price_version][supplier_id][product_id]
                    offer[price_version][product_id] = {"price": product['price'], "exp": product['exp'], "qty": self.data['basket'][product_id]['qty'], "present": True, "reason_if_absent": None}

            offer = {"supplier_id": supplier_id, "offer": offer, "timeout": datetime.now() + timedelta(minutes=5), "status": "active"}
            offer_id = (await db_add_offer(offer))['offer_id']
            self.data['offers'][offer_id] = offer
            await self.save({'of' + str(offer_id): offer})
            #### SEND OFFER ####

        self.data['filtered_pricelist'] = await remove_used(optimized_prices=self.data['optimized_prices'], filtered_pricelist=self.data['filtered_pricelist'], only_optimized_prices=True)
        # self.data['filtered_pricelist_id'] = (await db_add_filtered_pricelist(self.data))['filtered_pricelist_id']
        # await self.save({'fp' + str(self.data['filtered_pricelist_id']) : self.data['filtered_pricelist']})

        return await self.check_offers(self.data)

    async def check_offers(self, data):
        optimization_service_executor = OptimizationServiceExecutorClient()
        self.data = data
        active_left = False

        """Check offers status & set active_left to True if there are active offers"""
        for offer_id in self.data['offers'].keys():
            if (self.data['offers'][offer_id]['status'] == 'active'):
                if (datetime.now() > self.data['offers'][offer_id]['timeout']):
                    self.data['offers'][offer_id]['status'] = 'declined'
                else:
                    active_left = True

        if not active_left:
            """If no active offers ---> check which products are absent & leave only them in the filtered_pricelist"""
            absent_products = []
            for offer_id in self.data['offers'].keys():
                if self.data['offers'][offer_id]['status'] != 'confirmed':
                    continue
                offer = self.data['offers'][offer_id]['offer']

                for price_version in offer.keys():
                    for product_id in offer[price_version].keys():
                        if not offer[price_version][product_id]['present']:
                            absent_products.append(product_id)

            for price_version in self.data['filtered_pricelist'].keys():
                for supplier_id in self.data['filtered_pricelist'][price_version].keys():
                    self.data['filtered_pricelist'][price_version][supplier_id] = {k: v for k, v in
                                                                      self.data['filtered_pricelist'][price_version][
                                                                          supplier_id].items() if k in absent_products}
                self.data['filtered_pricelist'][price_version] = {k: v for k, v in self.data['filtered_pricelist'][price_version].items() if
                                                     len(v) > 0}

            self.data['filtered_pricelist'] = {k: v for k, v in self.data['filtered_pricelist'].items() if len(v) > 0}

            if len(self.data['filtered_pricelist']) > 0:
                self.data['optimized_prices'] = await optimization_service_executor.get_optimized_prices(
                    data=self.data['filtered_pricelist'])

                for supplier_id in self.data['optimized_prices'].keys():
                    offer = {}
                    for price_version in self.data['optimized_prices'][supplier_id].keys():
                        offer[price_version] = {}
                        for product_id in self.data['optimized_prices'][supplier_id][price_version]:
                            product = self.data['filtered_pricelist'][price_version][supplier_id][product_id]
                            offer[price_version][product_id] = {"price": product['price'], "exp": product['exp'],
                                                                "qty": self.data['basket'][product_id]['qty'],
                                                                "present": True, "reason_if_absent": None}

                    offer = {"supplier_id": supplier_id, "offer": offer,
                             "timeout": datetime.now() + timedelta(minutes=5), "status": "active"}
                    offer_id = (await db_add_offer(offer))['offer_id']
                    self.data['offers'][offer_id] = offer
                    await self.save({'of' + str(offer_id): offer})
                    #### SEND OFFER ####
                self.data['filtered_pricelist'] = await remove_used(optimized_prices=self.data['optimized_prices'],
                                                                    filtered_pricelist=self.data['filtered_pricelist'],
                                                                    only_optimized_prices=True)
                # self.data['filtered_pricelist_id'] = (await db_add_filtered_pricelist(self.data))['filtered_pricelist_id']
                # await self.save({'fp' + str(self.data['filtered_pricelist_id']) : self.data['filtered_pricelist']})

                return await self.check_offers(self.data)
            else:
                ## ___________________________________TRANSACTION________________________________________##
                pass
        else:
            return await self.check_offers(self.data)

    async def confirm_offer(self, data):
        offer = await self.get('of' + str(data['offer_id']))

        if offer['status'] == 'declined':
            return False

        offer['status'] = 'confirmed'
        await self.save({'of' + str(data['offer_id']): offer})
        return True

    """To prolong the timer"""
    async def prolong_offer(self, data):
        offer = await self.get('of' + str(data['offer_id']))

        if offer['status'] != 'active':
            return False

        offer['timeout'] += timedelta(minutes=data['minutes'])
        await self.save({'of' + str(data['offer_id']) : offer})
        return True

    async def decline_offer(self, data):
        offer = await self.get('of' + str(data['offer_id']))

        if offer['status'] == 'declined':
            return False

        offer['status'] = 'declined'
        await self.save({'of' + str(data['offer_id']): offer})
        return True
