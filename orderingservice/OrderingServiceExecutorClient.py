import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')

from modules.kinetic_core.AbstractClient import *
from modules.kinetic_core.AbstractExecutor import executor
from orderingservice.OrderingServiceExecutor import OrderingServiceExecutor

@executor(OrderingServiceExecutor)
class OrderingServiceExecutorClient(AbstractClient):

    @rpc
    async def order(self, data):
        pass

    @rpc
    async def prolong_offer(self, data):
        pass

    @rpc
    async def check_offers(self, data):
        pass

    @rpc
    async def confirm_offer(self, data):
        pass