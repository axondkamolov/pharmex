from modules.kinetic_core.Connector import db
import sys
import json
sys.path.append('x:/Pharmex/Pharmex/vita')

async def db_add_basket(data):
    return (await db.query(
        'insert into baskets(client_id, basket, created_at) values '
        '($1, $2, $3) returning basket_id, current_timestamp',
        (data['client_id'], json.dumps(data["basket"]), data['timestamp'])
    ))

async def db_add_offer(data):
    return (await db.query(
        'insert into offers(offer, timeout, status) values '
        '($1, $2, $3) returning offer_id, current_timestamp',
        (json.dumps(data["offer"]), data['timeout'], data['status'])
    ))

async def db_add_filtered_pricelist(data):
    return (await db.query(
        'insert into filtered_pricelists(filtered_pricelist) values '
        '($1) returning filtered_pricelist_id, current_timestamp',
        (json.dumps(data["filtered_pricelist"]))
    ))
# async def db_remove(data):
#     return (await db.query(
#         'update driver_stats set active = 0 where driver_id = $1'
#         ' returning active, current_timestamp',
#         (data["driver_id"])
#     ))
