import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')
from modules.kinetic_core.AbstractExecutor import *
from pharmaclient.PharmaExecutorDB import *
from datetime import timedelta
from preorderingservice.PreorderingServiceExecutorClient import PreorderingServiceExecutorClient
from orderingservice.OrderingServiceExecutorClient import OrderingServiceExecutorClient

class PharmaExecutor(AbstractExecutor):

    async def order(self, data):
        # This method is used to add new instance to the system
        """Generate basket and save to DB"""
        preorder_executor = PreorderingServiceExecutorClient()
        order_executor = OrderingServiceExecutorClient()
        
        self.data = {}
        self.data['client_id'] = 1
        self.data['basket'] = {
            101: {"qty": 1000, "exp": datetime.strptime("2019-02", "%Y-%m")},
            102: {"qty": 1500, "exp": datetime.strptime("2019-02", "%Y-%m")},
            114: {"qty": 1000, "exp": datetime.strptime("2019-02", "%Y-%m")}
        }
        self.data['price_versions'] = [25, 50]
        optimized_price_ranges = await preorder_executor.preorder(data=self.data)

        print(optimized_price_ranges)
        confirmation = input("Agree? (y/n)")

        if confirmation != 'y' and confirmation != 'Y':
            print("Bye")
            return

        self.data['max_cost'] = optimized_price_ranges[self.data['prepayment_percent']]['max_cost']
        self.data['timestamp'] = datetime.now()

        return await order_executor.order(data=self.data)
