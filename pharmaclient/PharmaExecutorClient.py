import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')
from modules.kinetic_core.AbstractClient import *
from modules.kinetic_core.AbstractExecutor import executor
from pharmaclient.PharmaExecutor import PharmaExecutor

@executor(PharmaExecutor)
class PharmaExecutorClient(AbstractClient):

    @rpc
    def order(self, data):
        """
        Adds preorder to database and redis
        :param data: Dict
        :return: Instance Id
        """
        pass
