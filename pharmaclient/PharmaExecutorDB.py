from modules.kinetic_core.Connector import db
import sys
import json
sys.path.append('x:/Pharmex/Pharmex/vita')

async def db_add_basket(data):
    return (await db.query(
        'insert into baskets(client_id, basket, created_at) values '
        '($1, $2, $3) returning basket_id, current_timestamp',
        (data['client_id'], json.dumps(data["basket"]), data['timestamp'])
    ))

async def db_confirm_price_ranges(data):
    return (await db.query(
        'update optimized_price_ranges set confirmed=$1 where optimized_price_ranges_id=$2'
        ' returning optimized_price_ranges_id, current_timestamp',
        (data['confirmed'], data['optimized_price_ranges_id'])
    ))

async def db_confirm_prices(data):
    return (await db.query(
        'update optimized_prices set confirmed=$1 where optimized_prices_id=$2'
        ' returning optimized_prices_id, current_timestamp',
        (data['confirmed'], data['optimized_prices_id'])
    ))

# async def db_remove(data):
#     return (await db.query(
#         'update driver_stats set active = 0 where driver_id = $1'
#         ' returning active, current_timestamp',
#         (data["driver_id"])
#     ))
