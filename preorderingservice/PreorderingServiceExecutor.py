import sys

# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')
from modules.kinetic_core.AbstractExecutor import *
from pricelist.PriceListExecutorClient import PriceListExecutorClient
from filterservice.FilterServiceExecutorClient import FilterServiceExecutorClient
from optimizer.OptimizationServiceExecutorClient import OptimizationServiceExecutorClient


class PreorderingServiceExecutor(AbstractExecutor):
    def __init__(self, task):
        super().__init__(task)

    async def preorder(self, data):
        self.data = {}
        price_list_executor = PriceListExecutorClient()
        filter_service_executor = FilterServiceExecutorClient()
        optimization_service_executor = OptimizationServiceExecutorClient()

        """Getting pricelist from pricelists holder"""
        self.data['basket_pricelist'] = await price_list_executor.get_basket_pricelist(data=data['basket'])

        """Filtering pricelist by expiry, rating, etc."""
        self.data['filtered_pricelist'] = await filter_service_executor.get_filtered_list(
            data={'basket_pricelist': self.data['basket_pricelist'], 'basket': data})

        """"Create a dict with best/worst pricelist and min/max costs"""
        self.data['optimized_price_ranges'] = {}

        self.data['optimized_price_ranges']['best_prices'] = await optimization_service_executor.get_optimized_prices(
            data=self.data['filtered_pricelist'])
        self.data['optimized_price_ranges']['min_cost'] = await get_cost(basket=data['basket'], optimized_prices=
        self.data['optimized_price_ranges']['best_prices'], filtered_pricelist=self.data['filtered_pricelist'])

        self.data['product_count'] = await get_product_count(filtered_pricelist=self.data['filtered_pricelist'])
        """Filter out all used sid:pid pairs to get a worse result"""
        self.data['filtered_pricelist'] = await remove_used(
            optimized_prices=self.data['optimized_price_ranges']['best_prices'],
            filtered_pricelist=self.data['filtered_pricelist'])

        """If yes then generate the worst pricelist and the corresponding max price"""
        self.data['optimized_price_ranges']['worst_prices'] = await optimization_service_executor.get_optimized_prices(
            data=self.data['filtered_pricelist'], flag=True)
        self.data['optimized_price_ranges']['max_cost'] = await get_cost(data['basket'],
                                                                         self.data['optimized_price_ranges'][
                                                                             'worst_prices'])

        return self.data['optimized_price_ranges']


"""Returns how many items of each product is left"""


async def get_product_count(filtered_pricelist):
    product_count = {}
    for price_version in filtered_pricelist.keys():
        for supplier_id in filtered_pricelist[price_version].keys():
            for product_id in filtered_pricelist[price_version][supplier_id].keys():
                if not product_id in product_count.keys():
                    product_count[product_id] = 0
                product_count[product_id] += 1
    return product_count


"""Returns the product set"""


async def get_product_set(filtered_pricelist):
    product_set = {}
    for price_version in filtered_pricelist.keys():
        for supplier_id in filtered_pricelist[price_version].keys():
            for product_id in filtered_pricelist[price_version][supplier_id].keys():
                if not product_id in product_set.keys():
                    product_set[product_id] = {}
                product_set[product_id][supplier_id, price_version] = \
                filtered_pricelist[price_version][supplier_id][product_id]['price']

    for product_id in product_set.keys():
        product_set[product_id] = sorted(product_set[product_id].items(), key=lambda item: item[1])
    return product_set


"""Returns a list of products having prices on the range of 5% of the best"""


async def get_products_to_remove(filtered_pricelist):
    product_set = get_product_set(filtered_pricelist)

    products_to_remove = {}
    for price_version in filtered_pricelist.keys():
        products_to_remove[price_version] = {}
        for supplier_id in filtered_pricelist[price_version].keys():
            products_to_remove[price_version][supplier_id] = []

    for product_id in product_set.keys():
        for ((supplier_id, price_version), price) in product_set[product_id][
                                                     :max(1, int(0.05 * len(product_set[product_id])))]:
            products_to_remove[price_version][supplier_id].append(product_id)

    return products_to_remove


"""Calculates the total cost of the basket with the given prices"""


async def get_cost(optimized_prices, filtered_pricelist, basket):
    total_cost = 0
    for supplier_id in optimized_prices.keys():
        for product_id, price_version in optimized_prices[supplier_id].items():
            total_cost += filtered_pricelist[str(price_version)][supplier_id][product_id]['price'] * basket[product_id][
                'qty']
    return total_cost


"""Removes from filtered_pricelist all sid:pid pairs in optimized_prices[plist_key]"""


async def remove_used(optimized_prices, filtered_pricelist, only_optimized_prices=False):
    products_to_remove = get_products_to_remove(filtered_pricelist)
    product_count = get_product_count(filtered_pricelist=filtered_pricelist)

    for supplier_id in optimized_prices.keys():
        for price_version in optimized_prices[supplier_id].keys():
            filtered_pricelist[price_version][supplier_id] = {k: v for k, v in
                                                              filtered_pricelist[price_version][supplier_id].items() if
                                                              (not k in optimized_prices[supplier_id][
                                                                  price_version]) or (product_count[k] == 1)}
            filtered_pricelist[price_version] = {k: v for k, v in filtered_pricelist[price_version].items() if v}
    filtered_pricelist = {k: v for k, v in filtered_pricelist.items() if v}

    if only_optimized_prices:
        return filtered_pricelist

    product_count = get_product_count(filtered_pricelist=filtered_pricelist)

    for price_version in filtered_pricelist.keys():
        for supplier_id in filtered_pricelist[price_version].keys():
            filtered_pricelist[price_version][supplier_id] = {k: v for k, v in
                                                              filtered_pricelist[price_version][supplier_id].items() if
                                                              (not k in products_to_remove[price_version][
                                                                  supplier_id]) or (product_count[k] == 1)}
        filtered_pricelist[price_version] = {k: v for k, v in filtered_pricelist[price_version].items() if v}
    filtered_pricelist = {k: v for k, v in filtered_pricelist.items() if v}

    return filtered_pricelist
