import sys

# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')

from modules.kinetic_core.AbstractClient import *
from modules.kinetic_core.AbstractExecutor import executor
from preorderingservice.PreorderingServiceExecutor import PreorderingServiceExecutor


@executor(PreorderingServiceExecutor)
class PreorderingServiceExecutorClient(AbstractClient):

    @rpc
    async def preorder(self, data):
        pass
