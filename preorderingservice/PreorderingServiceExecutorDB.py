from modules.kinetic_core.Connector import db
import sys
import json
sys.path.append('x:/Pharmex/Pharmex/vita')

async def db_add_basket(data):
    return (await db.query(
        'insert into baskets(client_id, basket, created_at) values '
        '($1, $2, $3) returning basket_id, current_timestamp',
        (data['client_id'], json.dumps(data["basket"]), data['timestamp'])
    ))
