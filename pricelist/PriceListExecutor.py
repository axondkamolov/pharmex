import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')

from modules.kinetic_core.AbstractExecutor import *
from pricelist.PriceListExecutorDB import *

class PriceListExecutor(AbstractExecutor):
    def __init__(self, task):
        super().__init__(task)
        
    async def get_basket_pricelist(self, data):
        """Generate pricelist and save to DB"""
        return await self.generate_basket_pricelist(data)

    async def generate_basket_pricelist(self, basket):
        """Generate pricelist"""
        basket_pricelist = {
            25: {
                1: [{"product_id": 101, "price": 1000, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100},
                    {"product_id": 102, "price": 2500, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100}],
                21: [{"product_id": 114, "price": 10000, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100},
                     {"product_id": 102, "price": 2550, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100}],
                33: [{"product_id": 101, "price": 1000, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100},
                     {"product_id": 114, "price": 10500, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100}],
                45: [{"product_id": 101, "price": 975, "exp": datetime.strptime("2018-12", "%Y-%m"), "qty": 2}]
            },
            50: {
                1: [{"product_id": 101, "price": 1000, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100},
                    {"product_id": 102, "price": 2500, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100}],
                21: [{"product_id": 114, "price": 10000, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100},
                     {"product_id": 102, "price": 2550, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100}],
                33: [{"product_id": 101, "price": 1000, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100},
                     {"product_id": 114, "price": 10500, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100}],
                45: [{"product_id": 101, "price": 975, "exp": datetime.strptime("2018-12", "%Y-%m"), "qty": 2}]
            },
            75: {
                1: [{"product_id": 101, "price": 1000, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100},
                    {"product_id": 102, "price": 2500, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100}],
                21: [{"product_id": 114, "price": 10000, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100},
                     {"product_id": 102, "price": 2550, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100}],
                33: [{"product_id": 101, "price": 1000, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100},
                     {"product_id": 114, "price": 10500, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100}],
                45: [{"product_id": 101, "price": 975, "exp": datetime.strptime("2018-12", "%Y-%m"), "qty": 100}]
            },
            100: {
                1: [{"product_id": 101, "price": 1000, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100},
                    {"product_id": 102, "price": 2500, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100}],
                21: [{"product_id": 114, "price": 10000, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100},
                     {"product_id": 102, "price": 2550, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100}],
                33: [{"product_id": 101, "price": 1000, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100},
                     {"product_id": 114, "price": 10500, "exp": datetime.strptime("2019-05", "%Y-%m"), "qty": 100}],
                45: [{"product_id": 101, "price": 975, "exp": datetime.strptime("2018-12", "%Y-%m"), "qty": 100}]
            }
        }

        return basket_pricelist