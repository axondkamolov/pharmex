import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')

from modules.kinetic_core.AbstractClient import *
from modules.kinetic_core.AbstractExecutor import executor
from pricelist.PriceListExecutor import PriceListExecutor

@executor(PriceListExecutor)
class PriceListExecutorClient(AbstractClient):

    @rpc
    async def get_basket_pricelist(self, data):
        pass

    @rpc
    async def generate_basket_pricelist(self, basket):
        pass