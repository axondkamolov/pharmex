import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')
from modules.kinetic_core.Connector import db
import json

async def db_add_pricelist(data):
    return (await db.query(
        'insert into basket_pricelists(basket_pricelist, basket_id) values '
        '($1, $2) returning basket_pricelist_id, current_timestamp',
        (json.dumps(data["basket_pricelist"]), data['basket_id'])
    ))