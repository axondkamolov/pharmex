import logging
from datetime import datetime
import copy

from modules.kinetic_core.AbstractExecutor import *
from sales.PriceExecutorDB import *


class PriceExecutor(AbstractExecutor):

    def __init__(self, task):
        super().__init__(task)
        self.uid = "product_id"
        self.params = {"product_id": None,
                       "price": None,
                       "price_discounted": None}

    async def add(self):
        for d in self.data:
            self.params[d] = self.data[d]
        result = await db_add(self.params)
        instance_id = result["product_id"]
        self.params[self.uid] = instance_id
        await self.save({"p"+str(instance_id): self.params})
        result = {self.uid: instance_id}
        return result

    async def get_one(self):
        instance_id = self.data["product_id"]
        instance = await self.get(["p"+str(instance_id)])
        return instance

    async def modify(self):
        instance_id = self.data["product_id"]
        params = await self.get(["p" + str(instance_id)])
        if params is None:
            logging.error("instance does not exist")
            return False
        for p in params:
            self.params[p] = params[p]
        for d in self.data:
            self.params[d] = self.data[d]
        result = await db_modify(self.params)
        for r in result:
            self.params[r] = result[r]
        await self.save({"p" + str(instance_id): result})

    async def remove(self):
        instance_id = self.data["product_id"]
        result = await db_remove(instance_id)
        await self.save({"p" + str(instance_id): result})