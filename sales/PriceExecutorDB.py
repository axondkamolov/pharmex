from modules.kinetic_core.Connector import db
import json

async def db_add(data):

    return (await db.query(
        'insert into price(product_id, price, price_discounted) values '
        '($1, $2, $3) returning product_id, current_timestamp',
        (data["product_id"], data["price"], data["price_discounted"])))

async def db_remove(instance_id):
    return (await db.query('delete from price where product_id=$1 returning product_id, current_timestamp',
                           (instance_id,)))

async def db_modify(data):

    return (await db.query('update price set price=$1, price_discounted=$2 where product_id=$3 returning price, price_discounted, current_timestamp',
        ((data["price"], data["price_discounted"], data["product_id"]))))