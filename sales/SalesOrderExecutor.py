import logging
from datetime import datetime
import copy

from modules.kinetic_core.AbstractExecutor import *
from sales.SalesOrderExecutorDB import *

from warehouse.StockExecutorClient import StockExecutorClient
from warehouse.WarehouseExecutorClient import WarehouseExecutorClient
from shipment.ShipmentExecutorClient import ShipmentExecutorClient

class SalesOrderExecutor(AbstractExecutor):

    def __init__(self, task):  #TODO change elsewhere
        super().__init__(task)
        self.uid = "order_id"
        self.params = {"order_id": None,
                       "customer_id": None,
                       "basket": {},
                       "bill_basket": None,
                       "payment_method": "cod",
                       "payment_confirmed": False,
                       "shipment_type": "1day",
                       "order_date": datetime.now(),
                       "address_id": None,
                       "latitude": None,
                       "longitude": None,
                       "address_string": None,
                       "comment": "",
                       "bill_shipment": None,
                       "bill_total": None,
                       "order_status": "initiated"
                       }


    async def add(self):
        # RPC TYPE
        # This method is used to create new order in DB, enter data in Redis
        for d in self.data:
            self.params[d] = self.data[d]
        result = await db_add(self.params)
        instance_id = result["order_id"]
        self.params[self.uid] = instance_id
        await self.save({"o"+str(instance_id): self.params})
        #print(self.params)
        # Send instance_id back to initiator of rpc
        result = {self.uid: instance_id}
        return result

        # Book items in basket
        #booking_executor = BookingExecutorClient(asyncio.get_event_loop())
        #await booking_executor.book_basket({"basket": self.params["basket"], self.uid: instance_id})
        # await booking_executor.reserve_basket({"basket": self.params["basket"], "order_id": order_id})

        # Create shipment and picking executors???

    async def get_one(self):
        instance_id = self.data["order_id"]
        print("o"+str(instance_id))
        instance = await self.get(["o"+str(instance_id)])
        print(instance)
        await self.publish(instance)


    async def process_order(self):
        # This method executes the following logic:
        # - check if whole order is available in all of warehouses
        # - if yes, use an optimizator to select the warehouse with lowest marginal cost
        # - if no, choose the one which is available
        # - if none of warehouses is available for single batch shipment, compose separate shipments from different warehouses
        # - makes booking as a result of logic outcome - booking modified qunantities and creates next necessary executors

        # - check if whole order is available in all of warehouses

        stock_executor = StockExecutorClient(asyncio.get_event_loop())#TODO change elsewhere
        product_availability = {}
        for product_id in self.data["basket"]:
            stock_by_product = await stock_executor.get_one({"product_id": product_id})
            product_availability[product_id] = stock_by_product

        warehouse_executor = WarehouseExecutorClient(asyncio.get_event_loop())
        warehouses = await warehouse_executor.get_all(None)
        product_availability_by_warehouse = {}
        for w in warehouses:
            warehouse_id = w["warehouse_id"]
            product_availability_in_warehouse = {}
            for product_id in self.data["basket"]:
                racks = product_availability[warehouse_id]
                quantity_in_warehouse = 0
                for rack_id in racks:
                    quantity_in_rack = racks[rack_id]
                    quantity_in_warehouse += quantity_in_rack
                product_availability_in_warehouse[product_id] = quantity_in_warehouse
            product_availability_by_warehouse[w] = product_availability_in_warehouse

        basket_availability_by_warehouse = {}
        for w in warehouses:
            fully_available = True
            for product_id in self.data["basket"]:
                demanded_quantity = self.data["basket"][product_id]
                available_quantity = product_availability_by_warehouse[w][product_id]
                if available_quantity < demanded_quantity:
                    fully_available = False
            if fully_available:
                basket_availability_by_warehouse[w] = True

        if len(basket_availability_by_warehouse) > 0:
            if len(basket_availability_by_warehouse) > 1:
                pass  # select among warehouses using marginal cost of shipping obtained via optimizer
                selected_warehouse_id = 1
            else:
                pass  # ship from one warehouse
                selected_warehouse_id = None
                for w in basket_availability_by_warehouse:
                    selected_warehouse_id = w

        else:
            pass  # whole basket is not available in any warehouse, so ship by product or product batches

            shipment_executor = ShipmentExecutorClient(asyncio.get_event_loop())

            products_to_be_shipped = copy.deepcopy(self.data["basket"])
            for w in product_availability_by_warehouse:
                warehouse_order = {}
                for product_id in products_to_be_shipped:
                    demanded_quantity = products_to_be_shipped[product_id]
                    if demanded_quantity > 0:
                        warehouse_order[product_id] = product_availability_by_warehouse[w]
                        products_to_be_shipped[product_id] -= product_availability_by_warehouse[w]
                # create new shipment order using warehouse_order[w]
                shipment_seed = self.params.copy()
                shipment_seed["basket"] = warehouse_order
                if shipment_seed["payment_method"] == "cod": # since there is cash on delivery option and there are more than one deliveries, customer will pay cash on LAST delivery
                    shipment_seed["payment_method"] = "cold"
                    #TODO either manager executor or shipment executor needs to decide which shipment is last and request payment accordingly
                shipment_seed["warehouse_id"] = w
                await shipment_executor.add(shipment_seed)

    async def remove(self):
        # EVENT TYPE
        # This method is used to modify existing order by marking completed field as True

        params = await self.get(self.data[self.uid])

        if params is None:
            logging.error("instance does not exist")
            return False
        for p in params:
            self.params[p] = params[p]
        params = self.params

        #TODO DO SMTH

        self.save(params)