import logging
from datetime import datetime
from modules.kinetic_core.AbstractExecutor import *
from shipment.ShipmentExecutorDB import *

class ShipmentExecutor(AbstractExecutor):

    def __init__(self, task):
        super().__init__(task)
        self.uid = "order_id"
        self.params = {"shipment_id": None,
                       "order_id": None,
                       "customer_id": None,
                       "basket": {},
                       "bill_basket": None,
                       "payment_method": "cod",
                       "payment_confirmed": False,
                       "shipment_type": "1day",
                       "order_date": datetime.now(),
                       "address_id": None,
                       "latitude": None,
                       "longitude": None,
                       "address_string": None,
                       "comment": "",
                       "bill_shipment": None,
                       "bill_total": None,
                       "warehouse_id": None
                       }


    async def add(self):
        # RPC TYPE
        # This method is used to create new order in DB, enter data in Redis

        for d in self.data:
            self.params[d] = self.data[d]
        order_id = await db_create_shipment(self.params)
        self.params[self.uid] = order_id
        await self.save(self.params)

        # Create shipment and picking executors


        # Send order_id back to initiator of rpc
        result = {"order_id": order_id}
        await self.publish(result)

    async def remove(self):
        # EVENT TYPE
        # This method is used to modify existing order by marking completed field as True

        params = self.get(self.data["order_id"])

        if params is None:
            logging.error("instance does not exist")
            return False
        for p in params:
            self.params[p] = params[p]
        params = self.params

        #TODO DO SMTH

        self.save(params)