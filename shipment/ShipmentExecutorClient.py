from modules.kinetic_core.AbstractClient import *
from modules.kinetic_core.AbstractExecutor import executor
from shipment.ShipmentExecutor import ShipmentExecutor


@executor(ShipmentExecutor)
class ShipmentExecutorClient(AbstractClient):

    @event
    async def add(self, data):
        """
        Creates new shipment
        :param data: Order Dict
        :return: None
        """
        pass

    @event
    def remove(self, data):
        """
        Cancels the shipment
        :param data: Empty
        :return: None
        """
        pass