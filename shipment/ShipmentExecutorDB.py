from modules.kinetic_core.Connector import db
import json

async def db_create_shipment(data):

    basket = json.dumps(data["basket"])
    print("create")

    return (await db.query(
        'insert into shipments(customer_id, basket, bill_basket, payment_method, payment_confirmed,'
        ' shipment_type, order_date, address_id, comment, bill_shipment, bill_total, order_status) values '
        '($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) returning order_id',
        (data["customer_id"], basket, data["bill_basket"], data["payment_method"], data["payment_confirmed"],
         data["shipment_type"], data["order_date"], data["address_id"], data["comment"], data["bill_shipment"],
         data["bill_total"], data["order_status"])))["shipment_id"]