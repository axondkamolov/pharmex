import sys

# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')

from modules.kinetic_core.AbstractExecutor import *
from supply.SupplierServiceExecutorDB import *


class SupplierServiceExecutor(AbstractExecutor):

    async def check_products_available(self, basket):
        # This method is used to add new instance to the system
        """Generate availability state list and save in DB"""
        return {"all_available": True, "basket": 1}
