import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')

from modules.kinetic_core.AbstractClient import *
from modules.kinetic_core.AbstractExecutor import executor
from supply.SupplierServiceExecutor import SupplierServiceExecutor

@executor(SupplierServiceExecutor)
class SupplierServiceExecutorClient(AbstractClient):

    @rpc
    def check_products_available(self, basket):
        """
        Adds preorder to database and redis
        :param data: Dict
        :return: Instance Id
        """
        pass