import sys
import asyncio

# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')
from modules.kinetic_core import Logger
from modules.kinetic_core.AbstractExecutor import executor
from modules.kinetic_core.QueueListener import QueueListener

from pharmaclient.PharmaExecutorClient import PharmaExecutorClient
from pharmaclient.PharmaExecutor import PharmaExecutor
from pricelist.PriceListExecutorClient import PriceListExecutorClient
from pricelist.PriceListExecutor import PriceListExecutor
from filterservice.FilterServiceExecutorClient import FilterServiceExecutorClient
from filterservice.FilterServiceExecutor import FilterServiceExecutor
from optimizer.OptimizationServiceExecutorClient import OptimizationServiceExecutorClient
from optimizer.OptimizationServiceExecutor import OptimizationServiceExecutor
from supply.SupplierServiceExecutorClient import SupplierServiceExecutorClient
from supply.SupplierServiceExecutor import SupplierServiceExecutor
from preorderingservice.PreorderingServiceExecutorClient import PreorderingServiceExecutorClient
from preorderingservice.PreorderingServiceExecutor import PreorderingServiceExecutor
from orderingservice.OrderingServiceExecutorClient import OrderingServiceExecutorClient
from orderingservice.OrderingServiceExecutor import OrderingServiceExecutor

main_loop = asyncio.get_event_loop()

"""Initializing listeners for all executors"""


@executor(PharmaExecutor)
class PharmaExecutorListener(QueueListener):
    async def parse(self, task):
        await PharmaExecutor(task).parse()


@executor(OrderingServiceExecutor)
class OrderingServiceExecutorListener(QueueListener):
    async def parse(self, task):
        await OrderingServiceExecutor(task).parse()


@executor(PreorderingServiceExecutor)
class PreorderingServiceExecutorListener(QueueListener):
    async def parse(self, task):
        await PreorderingServiceExecutor(task).parse()


@executor(SupplierServiceExecutor)
class SupplierServiceExecutorListener(QueueListener):
    async def parse(self, task):
        await SupplierServiceExecutor(task).parse()


@executor(PriceListExecutor)
class PriceListExecutorListener(QueueListener):
    async def parse(self, task):
        await PriceListExecutor(task).parse()


@executor(FilterServiceExecutor)
class FilterServiceExecutorListener(QueueListener):
    async def parse(self, task):
        await FilterServiceExecutor(task).parse()


@executor(OptimizationServiceExecutor)
class OptimizationServiceExecutorrListener(QueueListener):
    async def parse(self, task):
        await OptimizationServiceExecutor(task).parse()


"""Main function"""


async def dothistime():
    pharma_client = PharmaExecutorClient()

    print(await pharma_client.order(data=None))


# main_loop.create_task(dothistime())

main_loop.create_task(PharmaExecutorListener().register_listener(main_loop))
main_loop.create_task(PriceListExecutorListener().register_listener(main_loop))
main_loop.create_task(FilterServiceExecutorListener().register_listener(main_loop))
main_loop.create_task(OptimizationServiceExecutorrListener().register_listener(main_loop))
main_loop.create_task(SupplierServiceExecutorListener().register_listener(main_loop))
main_loop.create_task(PreorderingServiceExecutorListener().register_listener(main_loop))
main_loop.create_task(OrderingServiceExecutorListener().register_listener(main_loop))

main_loop.run_forever()
