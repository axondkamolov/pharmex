import logging
from datetime import datetime

from PIL import ImageDraw

from modules.kinetic_core.AbstractExecutor import *
from warehouse.BarcodeExecutorDB import *
import code128



class BarcodeExecutor(AbstractExecutor):
    def __init__(self, task):
        super().__init__(task)
        self.uid = "barcode_id"
        self.params = {"barcode_id": None,
                       "instance_id": None,
                       "instance_type": None}

    async def add(self):
        # EVENT TYPE
        # This method is used to add new instance to the system

        if len(self.data)>0:
            try:
                if self.data["instance_id"] is not None:
                    self.data["instance_id"] = str(self.data["instance_id"])
            except KeyError:
                pass
            try:
                if self.data["barcode_id"] is not None:
                    self.data["barcode_id"] = str(self.data["barcode_id"])
            except KeyError:
                pass

        for d in self.data:
            self.params[d] = self.data[d]
        instance_id = await db_add(self.params)

        self.params[self.uid] = instance_id

        await self.save({"b" + str(instance_id): self.params})

        result = {self.uid: instance_id}

        return result

    async def remove(self):

        instance_id = self.data["barcode_id"]

        result = await db_remove(instance_id)

        await self.save({"b" + str(instance_id): result})

    async def get_all(self):
        # RPC TYPE
        # This method is used to return list of instances
        instances = await self._list()
        return instances

    async def modify(self):

        # RPC TYPE
        # This method is used to modify atomic value of a variable

        instance_id = self.data["barcode_id"]
        params = await self.get(["b" + str(instance_id)])

        if params is None:
            logging.error("instance does not exist")
            return False
        for p in params:
            self.params[p] = params[p]

        for d in self.data:
            self.params[d] = self.data[d]

        result = await db_modify(self.params)

        for r in result:

            self.params[r] = result[r]
        await self.save({"b" + str(instance_id): result})

        return {"barcode_id": self.data["barcode_id"]}

    async def get_one(self):

        instance_id = self.data["barcode_id"]

        params = await self.get(["b" + str(instance_id)])
        return params

    async def generate_barcode_png(self):
        barcode_id = self.data["barcode_id"]
        barcode_image = code128.image(barcode_id)
        y=barcode_image.size[1]

        from PIL import Image
        def white_bg_square(img):
            "return a white-background-color image having the img in exact center"
            #size = (int(max(img.size)),) * 2
            size = (img.size[0],int(img.size[1]*1.3))
            layer = Image.new('RGB', size, (255, 255, 255))
            #layer.paste(img, tuple(map(lambda x: int((x[0] - x[1])), zip(size, img.size))))
            layer.paste(img, (0,0))
            return layer

        barcode_image = white_bg_square(barcode_image)
        barcode_image.resize((100, 100), Image.ANTIALIAS)

        from PIL import ImageDraw, ImageFont
        d = ImageDraw.Draw(barcode_image)
        txt = barcode_id
        fontsize = 1  # starting font size
        # portion of image width you want text width to be
        img_fraction = 0.40
        font = ImageFont.truetype("Arial.ttf", fontsize)
        while font.getsize(txt)[0] < img_fraction * barcode_image.size[0]:
            # iterate until the text size is just larger than the criteria
            fontsize += 1
            font = ImageFont.truetype("Arial.ttf", fontsize)
        # optionally de-increment to be sure it is less than criteria
        fontsize -= 1
        #fnt = ImageFont.truetype('/Library/Fonts/Arial.ttf', 15)
        fnt = ImageFont.truetype('Arial.ttf', fontsize)
        d.text(text=txt, xy=(int(barcode_image.size[0]/10), y),fill=(0,0,0),font=fnt)

        barcode_image.save("/var/www/vita/warehouse/barcodes_tmp/"+str(barcode_id)+".png")
