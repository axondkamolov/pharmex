from modules.kinetic_core.Connector import db

async def db_add(data):

    #if data["instance_id"] is not None:
    #    instance_id = str(data["instance_id"])
    #else:
    #    instance_id = None
    if data["barcode_id"] is None:
        return (await db.query(
            'insert into barcode(instance_id, instance_type) values ($1, $2)'
            ' returning barcode_id, current_timestamp', (data["instance_id"], data["instance_type"])))["barcode_id"]
    else:
        return (await db.query(
            'insert into barcode(barcode_id, instance_id, instance_type) values ($1, $2, $3)'
            ' returning barcode_id, current_timestamp', (data["barcode_id"], data["instance_id"], data["instance_type"])))[
            "barcode_id"]

async def db_remove(instance_id):
    return (await db.query('delete from barcode where barcode_id=$1 returning barcode_id, current_timestamp',
                           (instance_id,)))

async def db_modify(data):

    return (await db.query('update barcode set instance_id=$1, instance_type=$2 where barcode_id=$3 returning instance_id, instance_type, current_timestamp',
        ((str(data["instance_id"]), data["instance_type"], data["barcode_id"]))))