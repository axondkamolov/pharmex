import logging
import sys
# Add the ptdraft folder path to the sys.path list
sys.path.append('x:/Pharmex/Pharmex/vita')
from datetime import datetime
from modules.kinetic_core.AbstractExecutor import *
from warehouse.ProductsExecutorDB import *
from fuzzywuzzy import fuzz
from fuzzywuzzy import process


class ProductsExecutor(AbstractExecutor):
    def __init__(self, task):
        super().__init__(task)
        self.uid = "product_id"
        self.params = {"product_id": None,
                       "name": None,
                       "description": None,
                       "dosage_form": None,
                       "dosage_size": None,
                       "dosage_quantity": None,
                       "category": None,
                       "manufacturer": None,
                       "manufacturer_country": None,
                       "alternatives": None,
                       "association": None,
                       "available_next_day": None
                       }

    async def add(self):
        # EVENT TYPE
        # This method is used to add new instance to the system
        print(1)
        for d in self.data:
            self.params[d] = self.data[d]
        instance_id = await db_add(self.params)
        print(2)
        self.params[self.uid] = instance_id
        await self.save({"p" + str(instance_id): self.params})
        await self.save({"n" + str(self.params["name"]): self.params})
        print(3)
        result = {self.uid: instance_id}
        return result

    async def remove(self):
        instance_id = self.data["product_id"]
        params = await self.get(["p" + str(instance_id)])
        result = await db_remove(instance_id)
        await self.save({"n" + str(params["name"]): None})
        await self.save({"p" + str(instance_id): None})
        return True

    async def get_all(self):
        # RPC TYPE
        # This method is used to return list of instances
        instances = await self._list()
        return instances

    async def modify(self):
        # RPC TYPE
        # This method is used to modify atomic value of a variable
        instance_id = self.data["product_id"]
        params = await self.get(["p" + str(instance_id)])
        if params is None:
            logging.error("instance does not exist")
            return False
        for p in params:
            self.params[p] = params[p]
        for d in self.data:
            self.params[d] = self.data[d]
        result = await db_modify(self.params)
        for r in result:
            self.params[r] = result[r]
        await self.save({"p" + str(instance_id): result})
        await self.save({"n" + str(result["name"]): result})

        return True

    async def get_one(self):
        print(1)
        instance_id = self.data["product_id"]
        params = await self.get(["p" + str(instance_id)])
        return params

    async def get_one_name(self):
        name = self.data["name"]
        params = await self.get(["n" + str(name)])
        return params

    async def best_matches(self):
        name = self.data["name"]
        words = name.split()
        instances = await self._list()
        matches = {}
        for instance in instances:
            if instance[0] == "p":
                if len(instances[instance])>1:
                    max_ratio = 0
                    for word in words:
                        ratio = fuzz.ratio(word, instances[instance]["name"])
                        partial_ratio = fuzz.partial_ratio(word, instances[instance]["name"])
                        max_ratio = max(max_ratio,ratio,partial_ratio)
                    if max_ratio > 70:
                        matches[instances[instance]["name"]] = max_ratio
        sorted_matches = sorted(matches.items(), key=lambda kv: -kv[1])[:10]
        print(sorted_matches)
