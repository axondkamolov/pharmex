from modules.kinetic_core.Connector import db
import json

async def db_add(data):

    return (await db.query(
        'insert into products(name, description, dosage_form, dosage_size, dosage_quantity, category, manufacturer, manufacturer_country, alternatives,'
        ' association, available_next_day) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)'
        ' returning product_id, current_timestamp', (data["name"], data["description"], data["dosage_form"], data["dosage_size"],
        data["dosage_quantity"], data["category"],
        data["manufacturer"], data["manufacturer_country"], data["alternatives"], data["association"],
        data["available_next_day"])))["product_id"]

async def db_remove(instance_id):
    return (await db.query('delete from products where product_id=$1 returning product_id, current_timestamp',
                           (instance_id,)))

async def db_modify(data):

    return (await db.query('update products set name=$1, description=$2, dosage_form=$3, dosage_size=$4, dosage_quantity=$5, category=$6,'
                           ' manufacturer=$7,'
                           ' manufacturer_country=$8, alternatives=$9, association=$10, available_next_day=$11'
                           ' where product_id=$12 returning name, description, dosage_form, dosage_size, dosage_quantity, category, manufacturer,'
                           ' manufacturer_country, alternatives, association, available_next_day, current_timestamp',
        ((data["name"], data["description"], data["dosage_form"], data["dosage_size"], data["dosage_quantity"], data["category"],
        data["manufacturer"], data["manufacturer_country"], data["alternatives"], data["association"],
        data["available_next_day"], data["product_id"]))))