import logging
from datetime import datetime
from modules.kinetic_core.AbstractExecutor import *
from warehouse.StockExecutorDB import *


class StockExecutor(AbstractExecutor):
    def __init__(self, task):
        super().__init__(task)
        self.uid = "product_id"
        self.params = {"stock_id": None,
                       "product_id": None,
                       "warehouse_id": None,
                       "rack_id": None,
                       "shelf_id": None,
                       "quantity": None,
                       "expiry": None
                       }

    async def add(self):
        # EVENT TYPE
        # This method is used to add new instance to the system
        for d in self.data:
            self.params[d] = self.data[d]
        instance_id = await db_add(self.data)

        self.params["stock_id"] = instance_id

        await self.save({
            self.params["product_id"]: {
                self.params["warehouse_id"]: {
                    self.params["rack_id"]: self.params
                }
            }})

        await self.save(self.params)

        result = {"stock_id": instance_id}

        await self.publish(result)

    async def remove(self):
        params = await self.get(self.data[self.uid])

        if params is None:
            logging.error("instance does not exist")
            return False
        for p in params:
            self.params[p] = params[p]

        result = await db_remove(self.params)

        await self.save({
            self.params["product_id"]: {
                self.params["warehouse_id"]: {
                    self.params["rack_id"]: result
                }
            }})

        await self.save(result)

    async def get_all(self):
        # RPC TYPE
        # This method is used to return list of instances
        instances = await self._list()
        await self.publish(instances)

    async def modify(self):

        # RPC TYPE
        # This method is used to modify atomic value of a variable

        params = await self.get(self.data[self.uid]) # MODIFY

        if params is None:
            logging.error("instance does not exist")
            return False
        for p in params:
            self.params[p] = params[p]

        quantity = await db_modify(self.data)

        self.params["quantity"] = quantity

        await self.save({
            self.params["product_id"]: {
                self.params["warehouse_id"]: {
                    self.params["rack_id"]: self.params
                }
            }})

    async def get_one(self):

        params = await self.get(self.data[self.uid])  # MODIFY

        await self.publish(params)