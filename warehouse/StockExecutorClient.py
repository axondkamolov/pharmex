from modules.kinetic_core.AbstractClient import *
from modules.kinetic_core.AbstractExecutor import executor
from warehouse.StockExecutor import StockExecutor


@executor(StockExecutor)
class StockExecutorClient(AbstractClient):

    @rpc
    def add(self, data):
        """
        Adds instance to database and redis
        :param data: Dict
        :return: Instance Id
        """
        pass

    @event
    def remove(self, data):
        """
        Removes instance from redis and (marks as completed in DB / removes from DB)
        :param data: Dict
        :return: None
        """
        pass

    @rpc
    def get_all(self):
        """
        Returns list of instances
        :param data: None
        :return: Dict with list of instances
        """
        pass

    @rpc
    def get_one(self, data):
        """
        Returns single instance
        :param data: Dict
        :return: Dict with list of params
        """
        pass


