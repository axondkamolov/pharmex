from modules.kinetic_core.Connector import db
import json

async def db_add(data):

    return (await db.query(
        'insert into stock(product_id, quantity, expiry, rack_id, shelf_id, warehouse_id) values ($1, $2, $3, $4, $5, $6)'
        ' returning stock_id, current_timestamp', (data["product_id"], data["quantity"], data["expiry"], data["rack_id"],
        data["shelf_id"], data["warehouse_id"])))["stock_id"]

async def db_remove(data):

    return (await db.query('delete from stock where stock_id=$1 returning stock_id, current_timestamp',
                           (data["stock_id"],)))["stock_id"]

async def db_modify(data):

    return (await db.query('update stock set quantity=quantity+$1 where stock_id=$2 returning quantity, current_timestamp',
        (data["quantity"],data["stock_id"])))["quantity"]