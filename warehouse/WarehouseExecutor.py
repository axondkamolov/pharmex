import logging
from datetime import datetime
from modules.kinetic_core.AbstractExecutor import *
from warehouse.WarehouseExecutorDB import *

class WarehouseExecutor(AbstractExecutor):

    def __init__(self, task):
        super().__init__(task)
        self.uid = "warehouse_id"
        self.params = {"warehouse_id": None,
                       "name": None,
                       "latitude": None,
                       "longitude": None
                       }

    async def add(self):
        # EVENT TYPE
        # This method is used to add new warehouse to the system
        for d in self.data:
            self.params[d] = self.data[d]
        instance_id = await db_add(self.data)
        self.params[self.uid] = instance_id
        await self.save(self.params)

        result = {self.uid: instance_id}
        await self.publish(result)

    async def remove(self):
        params = await self.get(self.data[self.uid])

        if params is None:
            logging.error("instance does not exist")
            return False
        for p in params:
            self.params[p] = params[p]

        await self.delete(self.params)
        await db_remove(self.params)


    async def get_all(self):
        # RPC TYPE
        # This method is used to return list of warehouses
        instances = await self._list()
        await self.publish(instances)

