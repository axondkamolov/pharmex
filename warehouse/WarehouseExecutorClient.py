from modules.kinetic_core.AbstractClient import *
from modules.kinetic_core.AbstractExecutor import executor
from warehouse.WarehouseExecutor import WarehouseExecutor


@executor(WarehouseExecutor)
class WarehouseExecutorClient(AbstractClient):

    @rpc
    def add(self, data):
        """
        Adds instance to database and redis
        :param data: Dict
        :return: Instance Id
        """
        pass

    @event
    def remove(self, data):
        """
        Removes instance from redis and (marks as completed in DB / removes from DB)
        :param data: Dict
        :return: None
        """
        pass

    @rpc
    def get_all(self):
        """
        Returns list of instances
        :param data: None
        :return: Dict with list of instances
        """
        pass
