from modules.kinetic_core.Connector import db
import json

async def db_add(data):

    return (await db.query(
        'insert into warehouses(name, latitude, longitude) values ($1, $2, $3)'
        ' returning warehouse_id', (data["name"], data["latitude"], data["longitude"])))["warehouse_id"]

async def db_remove(data):

    await db.query('delete from warehouses where warehouse_id=$1', (data["warehouse_id"],))